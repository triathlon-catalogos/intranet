# Intranet de Catálogos

- Web de Producción: [http://catalogosdeportivos.pe](http://catalogosdeportivos.pe) 
- Web de Testing: [http://18.188.145.179](http://18.188.145.179) 

## Recursos

### Producción

- **Dominio:** [Punto.pe](https://punto.pe/)
- **Hosting:** [Digital Ocean](https://www.digitalocean.com/)

### Testing

- **Hosting:** [EC2 AWS](https://aws.amazon.com/)

## Stack de Tecnología

- Laravel
- Bootstrap
- jQuery

## Objetivo

Esta aplicación permite tener un canal de consulta de stock y realización de pedidos a las asesoras, para esto, hay 2 usuarios: 
**administrador** y **asesora**.

Los administradores tienen la capacidad de:

- Dar de alta a una asesora.
- Administrar los catálogos.
- Consultar stock para analizar los problemas que podría encontrar una asesora en su flujo normal.

Las asesoras tienen la función de:

- Validar la compra de su catálogo para ingresar a la intranet. Esto se hace cada vez que se renueve el catálogo.
- Consultar stock.
- Agregar productos a un carrito de compras.
- Agregar medios de comunicación ya sea para recojo en tienda o entrega a domicilio (calculando un flete).
- Descargar el último catálogo disponible en la web de Triathlon Catálogos.

> ## Nota
>
> Este documento solo indica las funcionalidades programadas 
> como requerimiento de Triathlon Catálogos, si algo nuevo fue
> programado por algún colaborador de la empresa en mención,
> puede actualizar este documento.
