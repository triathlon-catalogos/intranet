<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['prefix' => 'import'], function() {
	Route::post('/massive', 'ImporterController@importMassiveUsers');
});

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'IntranetController@index');
});

Route::group(['middlewareGroups' => 'auth'], function() {
	Route::group(['prefix' => 'users'], function() {
		Route::get('/verify', 'UserController@verify');
		Route::post('/validate', 'UserController@validateCatalog');
	});
	
	Route::group(['prefix' => 'import'], function() {
		Route::get('/', 'ImporterController@index');
		Route::post('/users', 'ImporterController@importUsers');
	});
	Route::group(['prefix' => 'products'], function() {
		Route::get('/stock', 'ProductController@stock');
	});
	Route::group(['prefix' => 'orders'], function() {
		Route::get('/history', 'OrderController@history');
		Route::get('/summary', 'OrderController@summary');
		Route::get('/checkout', 'OrderController@checkout');
		Route::post('/cart', 'OrderController@cart');
		Route::get('/thanks', 'OrderController@complete');
	});
	Route::group(['prefix' => 'catalog'], function() {
		Route::get('/', 'CatalogController@manage');
		Route::post('/', 'CatalogController@save');
		Route::put('/', 'CatalogController@update');
	});
});
