<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben ser de al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => '¡Tu contraseña ha sido reestablecida!',
    'sent' => 'Te hemos enviado un mensaje a tu correo electrónico con el link para reestablecer tu contraseña.',
    'token' => 'El token de reestablecer contraseña es inválido.',
    'user' => "No hemos podido encontrar un usuario con este correo electrónico.",

];
