@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="padding-top: 120px;">
        <div class="col-md-3">
            <div class="row">
                <div class="menu col-md-6 col-md-offset-3">
                    <li>
                        <a href="{{ url('users/verify') }}">Verificación de voucher</a>
                    </li>
                    <li>
                        <a href="{{ url('products/stock') }}">Consulta stock</a>
                    </li>
                    <li>
                        <a href="{{ url('orders/history') }}">Historial de Pedidos</a>
                    </li>
                    <li>
                        <a href="{{ url('orders/summary') }}">Confirmar pedido</a>
                    </li>
                    <li>
                        <a href="{{ url('import') }}">Importar Usuarios</a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}" 
                            onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Salir Hola!</a>
                    </li>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    Verificación de voucher
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
