@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 120px;">
    <div class="row">
        @if ($is_valid) 
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-4 menu"><a href="{{ url('products/stock') }}">
                    <div class="text-center color-h4">
                        <h4>CONSULTA STOCK</h4>
                    </div></a>
                </div>
                @if (Auth::user()->type == "customer") 
                <div class="col-md-4 menu"><a href="{{ url('orders/history') }}">
                    <div class="text-center color-h4">
                        <h4>HISTORIAL DE PEDIDOS</h4>
                    </div></a>
                </div>
                <div class="col-md-4 menu"><a href="{{ $catalog_url }}" target="_blank">
                    <div class="text-center color-h4">
                        <h4>DESCARGAR CATÁLOGO</h4>
                    </div></a>
                </div>
                @endif
                @if (Auth::user()->type == "employee")
                <div class="col-md-4 menu"><a href="{{ url('import') }}">
                    <div class="text-center color-h4">
                        <h4>IMPORTAR USUARIOS</h4>
                    </div></a>
                </div>
                <div class="col-md-4 menu"><a href="{{ url('catalog') }}">
                    <div class="text-center color-h4">
                        <h4>ADMINISTRAR CATÁLOGOS</h4>
                    </div></a>
                </div>
                @endif
            </div>
        </div>
        @else
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Validación de acceso a la plataforma</div>
                <div class="panel-body"  style="padding: 10px 25px 25px">
                    <div class="bienvenido" style="padding: 0 15px 20px">
                        <h3>¡Bienvenido!</h3>
                        <p>Debes ingresar el número de tickect de compra de tu Catálogo Deportivo Triathlon para acceder a la plataforma. <a href="#" data-toggle="modal" data-target="#ticket-example"><strong>Ver ejemplo</strong></a></p>
                    </div>
                    <form action="http://170.0.82.214/catalogos/api/receipt/" method="POST">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="form-group col-md-9">
                            <label for="nro-ticket">Ingresa Nro. Ticket:</label>
                            <input id="nro-ticket" type="text" name="nro-ticket" class="form-control">
                        </div>
                        <div class="form-group col-md-3" style="padding-top: 15px;">
                            <button id="validate-catalog" type="submit" class="btn btn-primary btn-buscar">VALIDAR</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
        <div class="col-md-10 col-md-offset-1">
            <div class="banner">
                <img src="http://catalogothn.com.pe/wp-content/uploads/2016/12/banner-padre-1.jpg" width="100%"/>
            </div>
        </div>
    </div>
</div>
<div id="ticket-example" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Nro Ticket de Ejemplo</h4>
            </div>
            <div class="modal-body">
                <img src="{{  asset('img/ticket-demo.png') }}" class="img-responsive img-center" alt="Ticket de Ejemplo" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        var loadPage = function () {
            $("#validate-catalog").click(function (e) {
                e.preventDefault();
                var nroTicket = $("#nro-ticket").val().trim();
                var $form = $(this).parents("form");
                var validateUrl = $form.attr("action");
                var customerNo = "{{ Auth::user()->extid }}";
                console.log(customerNo);
                $.getJSON(validateUrl + nroTicket + "/" + customerNo)
                    .done(function (response) {
                        if (response == null) {
                            swal("Oops...", "No se ha encontrado el ticket ingresado.", "error");
                            return;
                        } else {
                            $.post("{{ url('users/validate') }}", {
                                "_token": $("#token").val(),
                                receipt: response[0]
                            }).done(function (res) {
                                if (res.result === "failed") {
                                    swal("Oops...", res.error, "error");
                                } else {
                                    swal({
                                        title: "Genial!",
                                        text: "Gracias por validar la compra del catálogo, ahora puede disfrutar el acceso a la plataforma.",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: false
                                    },
                                    function(){
                                        window.location.reload();
                                    });
                                }
                            }).fail(function (err) {
                                console.log("error", err);
                            })
                        }
                    })
                    .fail(function (error) {
                        console.log("error", error);
                            swal("Oops...", "Ha ocurrido un error, vuelva a intentar y si persiste contacte a alguien de soporte por favor.", "error");
                    })
            });
        };
        $(document).ready(loadPage);
    </script>
@endsection