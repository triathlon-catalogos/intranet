@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 100px;">
    <h2 class="text-center titulo">Administrador de Catálogos</h2>
    <button type="button" class="btn btn-primary new-catalog" data-toggle="modal" data-target="#catalog-modal">
      Nuevo Catálogo
    </button>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Fecha de Lanzamiento</th>
                <th>Fecha de Vencimiento</th>
                <th>Código de Producto (RMS)</th>
                <th>-</th>
            </tr>
        </thead>
        <tbody id="catalog-list">
            @foreach ($catalogs as $catalog)
                <tr id="catalogo-{{ $catalog->id }}">
                    <th class="name">{{ $catalog->name }}</th>
                    <td class="release_date">{{ $catalog->release_date }}</td>
                    <td class="due_date">{{ $catalog->due_date }}</td>
                    <td class="product_code">{{ $catalog->product_code }}</td>
                    <td>
                        <button type="button" class="btn btn-default edit-catalog" data-toggle="modal" data-target="#catalog-modal" data-catalog-id="{{ $catalog->id }}">Editar</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!-- Modal -->
    <div class="modal fade" id="catalog-modal" tabindex="-1" role="dialog" aria-labelledby="catalog-title">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="catalog-title">Nuevo Catálogo</h4>
                </div>
                <div class="modal-body">
                    <form id="catalog-form" action="{{ url('catalog') }}" method="POST">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control" id="name" placeholder="Nombre de Catálogo">
                        </div>
                        <div class="form-group">
                            <label for="release-date">Fecha de Lanzamiento</label>
                            <input type="date" class="form-control" id="release-date" placeholder="Fecha de Lanzamiento">
                        </div>
                        <div class="form-group">
                            <label for="due-date">Fecha de Vencimiento</label>
                            <input type="date" class="form-control" id="due-date" placeholder="Fecha de Vencimiento">
                        </div>
                        <div class="form-group">
                            <label for="product-code">Código de Producto</label>
                            <input type="text" class="form-control" id="product-code" placeholder="Código de Producto en RMS">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button id="save-catalog" type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript">
        var catalogListTemplate = "<tr id='catalogo-__id__'>";
        catalogListTemplate += "<th class='name'>__name__</th>";
        catalogListTemplate += "<td class='release_date'>__releaseDate__</td>";
        catalogListTemplate += "<td class='due_date'>__dueDate__</td>";
        catalogListTemplate += "<td class='product_code'>__productCode__</td>";
        catalogListTemplate += "<td><button type='button' class='btn btn-default edit-catalog' data-toggle='modal'";
        catalogListTemplate += "data-target='#catalog-modal' data-catalog-id='__id__'>Editar</button></td>"
        catalogListTemplate += "</tr>";
        
        var loadPage = function () {
            $(".new-catalog").click(setNewCatalog);
            $("#catalog-list").on("click", ".edit-catalog", setEditCatalog);
            $("#save-catalog").click(saveCatalog);
        };
        
        var setNewCatalog = function() {
            $("#catalog-modal").attr("data-role", "new");
        };
        
        var setEditCatalog = function(e) {
            $("#catalog-modal").attr("data-role", "edit");
            var $catalogRow = $(this).parents("tr");
            var catalog = {
                id: $(this).attr("data-catalog-id"),
                name: $catalogRow.find(".name").text(),
                releaseDate: $catalogRow.find(".release_date").text(),
                dueDate: $catalogRow.find(".due_date").text(),
                productCode: $catalogRow.find(".product_code").text()
            };
            
            var $catalogForm = $("#catalog-form");
            $catalogForm.find("#name").val(catalog.name);
            $catalogForm.find("#release-date").val(catalog.releaseDate);
            $catalogForm.find("#due-date").val(catalog.dueDate);
            $catalogForm.find("#product-code").val(catalog.productCode);
            $("#save-catalog").attr("data-catalog-id", catalog.id);
        };
        
        var saveCatalog = function(e) {
            e.preventDefault();
            
            var { name, releaseDate, dueDate, productCode } = getFormData();
            if (name.length === 0 || productCode.length === 0) {
                swal("Oops...", "Debes ingresar todos los campos requeridos!", "error");
                return;
            }
            
            var $form = $("#catalog-form");
            var url = $form.attr("action");
            $(this).attr("disabled", "true");
            
            var role = $("#catalog-modal").attr("data-role");
            if (role === "new") {
                newCatalog(url, { name, releaseDate, dueDate, productCode });
            } else {
                var id = $("#save-catalog").attr("data-catalog-id");
                editCatalog(url, { id: parseInt(id), name, releaseDate, dueDate, productCode });
            }
        }
        
        var newCatalog = function (url, data = { id: -1, name: '', releaseDate: new Date(), dueDate: new Date(), productCode: '' }) {
            // var store = $("#store").val();
            
            $.post(url, {
                "_token": $("#token").val(),
                "catalog": data
            }).done(function (response) {
                if (response.result == "success") {
                    var catalog = response.data;
                    $("#catalog-list").append(catalogListTemplate
                        .replace(/__id__/g, catalog.id)
                        .replace("__name__", catalog.name)
                        .replace("__releaseDate__", catalog.release_date)
                        .replace("__dueDate__", catalog.due_date)
                        .replace("__productCode__", catalog.product_code));
                    $("#save-catalog").removeAttr("disabled");
                    $("#catalog-modal").modal("hide");
                }
            }).fail(function (error) {
                $("#save-catalog").removeAttr("disabled");
                swal("Error", "Un error ha ocurrido, vuelva a intentar y si falla nuevamente, contacte a soporte.", "error");
            });
        };
        
        var editCatalog = function (url, data = { name: '', releaseDate: new Date(), dueDate: new Date(), productCode: '' }) {
            $.ajax(url, {
                method: 'PUT',
                data: {
                    "_token": $("#token").val(),
                    "catalog": data
                }
            }).done(function (response) {
                if (response.result == "success") {
                    var catalog = response.data;
                    $(`#catalogo-${catalog.id}`).replaceWith(catalogListTemplate
                        .replace(/__id__/g, catalog.id)
                        .replace("__name__", catalog.name)
                        .replace("__releaseDate__", catalog.release_date)
                        .replace("__dueDate__", catalog.due_date)
                        .replace("__productCode__", catalog.product_code));
                    $("#save-catalog").removeAttr("disabled");
                    $("#catalog-modal").modal("hide");
                }
            }).fail(function (error) {
                $("#save-catalog").removeAttr("disabled");
                swal("Error", "Un error ha ocurrido, vuelva a intentar y si falla nuevamente, contacte a soporte.", "error");
            });
        };
        
        var getFormData = function () {
            var name = $("#name").val().trim();
            var releaseDate = $("#release-date").val();
            var dueDate = $("#due-date").val();
            var productCode = $("#product-code").val().trim();
            
            return {
                name,
                releaseDate,
                dueDate,
                productCode
            };
        };
        
        $(document).ready(loadPage);
    </script>
@endsection
