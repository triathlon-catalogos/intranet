@extends('layouts.app')

@section('content')
<style>
    .btn{
        width: 100%;
    }
    .recordar{
        font-size: 13px !important;
    }
    .buttons{
        margin: 30px 0 20px;
    }
    .mt-50{
        margin-top: 50px;
    }
    .mt-30{
        margin-top: 30px;
    }
    .width-130{
        width: 130px;
    }
    .celular{
        font-size: 15px !important;
        margin-top: 20px;
    }
    .detalle-pedido p{
        font-size: 15px !important;
        line-height: 1.2em !important;
        max-width: 650px;
        margin: 10px auto;
    }
</style>
<div class="container">
    <div class="row" style="padding-top: 120px;">
        <div class="breadcrumb">
            <a href="{{ url('orders/summary') }}"><< Volver</a>
        </div>
        <div class="col-md-12 direccion">
            <div class="jumbotron">
                <p>Selecciona si retiras tu pedido en tienda o si deseas el envío a domicilio:</p>
                <div class="row buttons">
                    <div class="col-sm-4 col-sm-offset-2">
                        <button type="button" class="btn btn-success btn-recojo-tienda">RECOJO EN TIENDA</button>
                    </div>
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-envio-domicilio">ENVÍO A DOMICILIO</button>
                    </div>
                </div>
                <p class="recordar text-center">Recuerda que el envío a domicilio tiene un costo de flete por producto.</p>
                <div id="recojo-tienda" class="row mt-50 text-center info-cliente" style="display:none;">
                    <p>Selecciona en qué tienda recogerás tus productos:</p>
                    <form action="{{ url('orders/thanks') }}" method="post" class="form-group col-md-4 col-md-offset-4">
                        {{ csrf_field() }}
                        <div class="dropdown">
                            <select id="store" class="form-control"></select>
                        </div>
                        <p class="celular">Digita tu número de celular:</p>
                        <input type="tel" id="phone" class="form-control text-center">
                        <div>
                            <button type="submit" class="btn btn-primary mt-30 width-130 btn-pagar-orden">CONFIRMAR</button>
                        </div>
                    </form>
                </div>
                <div id="envio-domicilio" class="container info-cliente" style="display:none;">
                    <form action="{{ url('order/thanks') }}" method="post">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label>Departamento</label>
                                <div class="dropdown">
                                    <select id="region" class="form-control">
                                        <option value="0">Selecciona un departamento</option>
                                        @foreach($regions as $region)
                                            <option value="{{ $region->id }}">{{ $region->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Provincia</label>
                                <div class="dropdown">
                                    <select id="province" class="form-control">
                                        <option value="0">Selecciona una provincia</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Distrito</label>
                                <div class="dropdown">
                                    <select id="district" class="form-control">
                                        <option value="0">Selecciona un distrito</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label>Especifique su dirección</label>
                                <input type="text" id="address" class="form-control"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Referencia</label>
                                <input type="text" id="reference" class="form-control"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-offset-4 col-md-4" style="padding-top: 15px;">
                                <button type="submit" data-flete="1" class="btn btn-primary btn-buscar btn-pagar-orden">GUARDAR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var nearRegions = [
        '150000', // Lima
        '070000', // Callao
    ];
    
    var nearProvinces = [
        '150100', // Lima
        '070100', // Provincia Constitucional del Callao
    ];

    var loadPage = function () {
        $(".btn-recojo-tienda").click(recojoTienda);
        $(".btn-envio-domicilio").click(entregaDomicilio);
        $(".btn-pagar-orden").click(pagarOrden);
        $("#region").on('change', obtenerProvincias);
        $("#province").on('change', obtenerDistritos);
        getStores();
    };
    
    var recojoTienda = function () {
        $("#envio-domicilio").fadeOut(10);
        $("#recojo-tienda").fadeIn(1000);
        sessionStorage.setItem('isDelivery', 'false');
    };
    
    var entregaDomicilio = function () {
        $("#recojo-tienda").fadeOut(10);
        $("#envio-domicilio").fadeIn(1000);
        sessionStorage.setItem('isDelivery', 'true');
    };
    
    var pagarOrden = function(e) {
        e.preventDefault();
        $(".btn-recojo-tienda").attr("disabled", true);
        $(".btn-envio-domicilio").attr("disabled", true);
        $(this).parents(".info-cliente").addClass("hidden");
        placeOrder();
        window.sessionStorage.clear();
    };
    
    var getStores = function() {
        $.getJSON("http://170.0.82.214/test/api/store")
        .done(function (data) {
            data.forEach(function(store) {
               $("#store").append("<option value='" + store.StoreNo + "'>" + store.StoreName + "</option>"); 
            });
        })
        .fail(function (error) {
            swal("Oops...", "Ocurrió un error al obtener el listado de tiendas!", "error");
        });
    };
    
    var saveOrder = function(payload) {
        if (payload.success) {
            payload._token = $('meta[name="csrf-token"]').attr('content');
            return $.post('/api/order', payload.data)
        }
    };
    
    var completeOrder = function(response) {
        if (response.result === "success") {
            window.location.href = "{{ url('orders/thanks') }}";
        } else {
            alert('Ha ocurrido un error, contacte a algún colaborador de Triathlon Catálogos');
        }
    }
    
    var placeOrder = function() {
        var data = getOrderData();
        console.log(data);
        $.post("http://170.0.82.214/test/api/order", data)
        .then(function (response) {
            data.ExtId = response;
            return { success: true, data: data };
        })
        .then(saveOrder)
        .then(completeOrder)
        .fail(function (error) {
            console.error('err', error);
        });
    };
    
    var getOrderData = function() {
        var lines = [];
        
        @for($i = 0; $i < count($products); $i++)
            lines.push({
                SKU: {{ $products[$i]['SKU'] }},
                Qty: {{ $products[$i]['Qty'] }},
                RetailPrice: {{ $products[$i]['RetailPrice'] * 0.82 }},
                RetailPriceWTax: {{ $products[$i]['RetailPrice'] }},
                ExtRetailPrice: {{ $products[$i]['RetailPrice'] * $products[$i]['Qty'] * 0.82 }},
                ExtRetailPriceWTax: {{ $products[$i]['RetailPrice'] * $products[$i]['Qty'] }},
                TaxAmount: {{ $products[$i]['RetailPrice'] * $products[$i]['Qty'] * 0.18  }}
            });
        @endfor
        
        var shipping = getShippingTotal();
        
        return {
            CustomerNo: "{{ $customer }}",
            OrderDate: formatDate(new Date()),
            ShippingTotal: shipping,
            SubTotal: {{ $payTotal * 0.82 }},
            SubTotalWTax: {{ $payTotal }},
            TaxTotal: {{ $payTotal * 0.18 }} + shipping,
            PayTotal: {{ $payTotal }},
            LineCount: {{ count($products) }},
            Notes: getOrderNotes(),
            Lines: lines
        };
    };
    
    var formatDate = function(orderDate) {
        var date = {
            year: orderDate.getFullYear(),
            month: orderDate.getMonth() + 1,
            day: orderDate.getDate(),
            hours: orderDate.getHours(),
            minutes: orderDate.getMinutes(),
            seconds: orderDate.getSeconds()
        };
        
        var { year, month, day, hours, minutes, seconds } = date;
        
        return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    };
    
    var getFreight = function(quantity, destination) {
        var freightTotal = 0;
        
        var currentFreight = freights.find(function(freight) {
            return quantity >= freight.min_quantity  && quantity <= freight.max_quantity; 
        });
        
        if (nearRegions.indexOf(destination.region) > -1 && nearProvinces.indexOf(destination.province) > -1) {
            freightTotal = currentFreight.near_destination_price;
        } else {
            freightTotal = currentFreight.far_destination_price;
        }
        
        return freightTotal;
    };
    
    var getShippingTotal = function() {
        var isDelivery = sessionStorage.getItem("isDelivery") === "true";
        
        if (isDelivery) {
            return Number(getFreight(totalQty, { 
                region: $("#region").val(), 
                province: $("#province").val() 
            }));
        }
        
        return 0;
    }
    
    var getOrderNotes = function() {
        var isDelivery = sessionStorage.getItem("isDelivery") === "true";
        var notes = '';
        
        if (isDelivery) {
            var address = $('#address').val();
            var region = $('#region').find('option:selected').text();
            var province = $('#province').find('option:selected').text();
            var district = $('#district').find('option:selected').text();
            var reference = $('#reference').val();
            
            notes = `Dirección: ${address}, ${district}, ${province} - ${region}. Referencia: ${reference}`;
        } else {
            var phone = $('#phone').val();
            notes = `Teléfono de contacto: ${phone}`;
        }
        
        return notes;
    };
    
    var obtenerProvincias = function(e) {
      var regionId = $(this).val();
      $("#province").empty();
      
      $.getJSON("{{ url('/api/provinces') }}", {
          "region_id": regionId 
      }).done(function (response) {
          var provinces = response.data;
          
          if (provinces) {
              $("#province").append("<option value='0'>Seleccione una provincia</option>");
              response.data.forEach(function(province) {
                  $("#province").append(`<option value='${province.id}'>${province.name}</option>`);
              });
          } else {
              $("#province").append("<option value='-1'>No existe provincia</option>");
          }
      }).fail(function (error) {
          $("#province").append("<option value='-1'>No existe provincia</option>");
      });
    };
    
    var obtenerDistritos = function(e) {
      var regionId = $("#region").val();
      var provinceId = $(this).val();
      $("#district").empty();
      
      $.getJSON("{{ url('/api/districts') }}", {
          "region_id": regionId,
          "province_id": provinceId,
      }).done(function (response) {
          var districts = response.data;
          
          if (districts) {
              $("#district").append("<option value='0'>Seleccione un distrito</option>");
              districts.forEach(function(district) {
                  $("#district").append(`<option value='${district.id}'>${district.name}</option>`);
              });
          } else {
              $("#district").append("<option value='-1'>No existe distritos</option>");
          }
      }).fail(function (error) {
          $("#district").append("<option value='-1'>No existe distritos</option>");
      });
    };
    
    $(document).ready(loadPage);
</script>
@endsection