@extends('layouts.app')

@section('content')
<style>
    .jumbotron{
        padding: 35px 50px;
    }
    .product{
        background-color: #fff;
        border-radius: 5px;
        padding: 20px 10px 10px;
        margin: 15px 0;
    }
    .carrito-text p{
        font-size: 14px;
        margin: 5px 0;
    }
    .title{
        font-size: 12px;
        margin-bottom: 10px;
        display: inline-block;
    }
    .cantidad{
        max-width: 70px;
        margin: 20px auto 0;
    }
    .precio{
        margin-top: 15px;
        font-size: 25px;
    }
    .btn-borrar{
        margin-top: 45px;
        width: 40px;
    }
</style>
<div class="container">
    <div class="row" style="padding-top: 120px;">
        <div class="breadcrumb">
            <a href="{{ url('products/stock') }}"><< Volver</a>
        </div>
        <h2 class="text-center titulo">CARRITO DE COMPRAS</h2>
        <div class="col-md-12">
            <div class="jumbotron">
                @if ($products)
                    <div class="with-products">
                        @php ($total = 0) 
                        @foreach($products as $product)
                            @php ($total += ($product["Qty"] * $product["RetailPrice"]))
                        <div class="row product">
                            <div class="col-sm-5 carrito-text">
                                <strong class="title">DETALLE DEL PRODUCTO: <span class="sku">{{ $product["SKU"] }}</span></strong>
                                <p>{{ $product["ProductDescription"] }}</p>
                                <p><strong>MARCA: </strong>{{ $product["Brand"] }}</p>
                                <p><strong>TALLA: </strong>{{ $product["SizeCode"] }}</p>
                            </div>
                            <div class="col-sm-2 text-center">
                                <strong class="title">CANTIDAD:</strong>
                                <div>
                                    <select name="qty-{{ $product['SKU']}}" id="qty-{{ $product['SKU'] }}" class="form-control qty">
                                        @for($i = 1; $i <= $product["Stock"]; $i++)
                                            @if ($i == $product["Qty"]) 
                                                <option value="{{ $i }}" selected>{{ $i }}</option>
                                            @else
                                                <option value="{{ $i }}" >{{ $i }}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3 text-center">
                                <strong class="title">PRECIO UNITARIO:</strong>
                                <p class="precio">S/. <span class="precio-venta">{{ $product["RetailPrice"] }}</span></p>
                            </div>
                            <div class="col-sm-2 text-center">
                                <button type="submit" class="btn btn-danger btn-borrar">X</button>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="no-products hidden">
                        <p>Su carrito está vacío :(</p>
                        <p><a href="{{ url('products/stock') }}">Volver</a></p>
                    </div>
                @else
                    <div class="no-products">
                        <p>Su carrito está vacío :(</p>
                        <p><a href="{{ url('products/stock') }}">Volver</a></p>
                    </div>
                @endif
            </div>
        </div>
        @if ($products)
        <div class="col-md-12 with-products">
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-3 col-md-offset-3">
                        Total a pagar:
                    </div>
                    <div class="col-md-2">
                        <p style="font-size: 15px"><strong>S/. <span class="paytotal">{{ $total }}</span></strong></p>
                    </div>
                    <div class="col-md-4">
                        <form id="confirm-order-form" action="{{ url('orders/checkout') }}">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <button type="submit" id="confirm-order" class="btn btn-success btn-buscar">CONFIRMAR</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@section('script')
    @if ($products)
    <script type="text/javascript">
        var products = JSON.parse(window.sessionStorage.getItem("cart")).products;
        var loadPage = function () {
            $("#confirm-order-form").on("submit", confirmOrder);
            $(".btn-borrar").on("click", removeItem);
            $(document).on("change", ".qty", updateCart);
        };
        
        var confirmOrder = function(e) {
            e.preventDefault();
            var url = $(this).attr("action");
            var $button = $(this).find("button[type='submit']");
            $button.attr("disabled");
            
            var cart = {
                customer: "{{Auth::user()->extid}}",
                products: products
            };
            
            window.sessionStorage.setItem("cart", JSON.stringify(cart));
            
            $.post("{{ url('orders/cart') }}", { 
                _token: $("#token").val(),
                cart: cart
            }).done(function (response) {
                console.log(response);
                window.location.href = url;
            }).fail(function (error) {
                $button.removeAttr("disabled");
                console.log("error", error);
            });
        };
        
        var removeItem = function(e) {
            // Remove product from UI
            var $row = $(this).parents('.product');
            $row.remove();
            
            // Remove product from array in memory
            var skuToRemove = $row.find('.sku').text();
            
            var productIndex = products.findIndex(function(product) {
                return product.SKU == skuToRemove;
            });
            
            products.splice(productIndex, 1);
            
            // Remove product from session storage
            updateTotal();
        };
        
        var updateCart = function() {
            var sku = $(this).parents(".product").find(".sku").text();
            
            var foundProduct = products.find(function (product) {
                return product.SKU == sku;
            });
            
            foundProduct.Qty = parseInt($(this).val());
            
            updateTotal();
        };
        
        var updateTotal = function() {
            var payTotal = products.reduce(function (last, current) {
                return last + (current.Qty * current.RetailPrice);
            }, 0);
            
            
            $(".paytotal").text(payTotal);
            
            var cart = {
                customer: "{{Auth::user()->extid}}",
                products: products
            };
            
            window.sessionStorage.setItem("cart", JSON.stringify(cart));
            
            $.post("{{ url('orders/cart') }}", { 
                _token: $("#token").val(),
                cart: cart
            }).done(function (response) {
                console.log(response);
            }).fail(function (error) {
                console.log("error", error);
            });  
            
            if (products.length == 0) {
                $('.with-products').remove();
                $('.no-products').removeClass('hidden');
            }
        };
        
        $(document).ready(loadPage);
    </script>
    @endif
@endsection