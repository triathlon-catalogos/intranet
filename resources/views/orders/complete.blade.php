@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="padding-top: 120px;">
        <div class="col-md-12 direccion">
            <div class="jumbotron">
                <div id="mensaje-final" class="row mt-50 text-center detalle-pedido">
                    <p><strong>¡Muchas gracias por completar tu pedido!</strong></p>
                    <p>Código de pedido: <strong>489067</strong></p>
                    <p>Tienes 24 horas a partir de ahora para realizar el <strong>depósito</strong> 
                    en el <strong>BCP</strong> a la cuenta recaudadora <strong>No 191 1854974009</strong>, 
                    caso contrario tu pedido será anulado. </p>
                    <p class="flete hidden">El costo de flete es: <strong>S/20</strong></p>
                    <p>El monto total a cancelar es: <strong>S/ <span class="total">{{ $payTotal }}</span></strong></p>
                    <p>Una vez realizado el depósito debe enviar su voucher de confirmación a 
                    <a href="mailto:catalogo@triathlon-sport.com">catalogo@triathlon-sport.com</a> 
                    y/o enviar una foto del mismo al whatsapp <strong>No. 998224447</strong></p>
                    <div class="breadcrumb">
                        <a href="{{ url('/') }}">Volver al inicio</a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    var loadPage = function () {
    };
    
    $(document).ready(loadPage);
</script>
@endsection