@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row"  style="padding-top: 120px;">
        <h2 class="text-center titulo">Historial de pedidos</h2>
        @foreach($orders as $order)
            <div class="pedido">
                <div class="row">
                    <div class="col-md-4">
                        <h3>PEDIDO {{ $order->id }}</h3>
                    </div>
                    <div class="col-md-4 col-md-offset-4">
                        <button type="submit" class="btn btn-primary btn-buscar">POR CONFIRMAR</button>
                    </div>
                </div>
            </div>
        @endforeach
        <!--<div class="pedido">-->
        <!--    <div class="row">-->
        <!--        <div class="col-md-4">-->
        <!--            <h3>PEDIDO PCDT170004</h3>-->
        <!--        </div>-->
        <!--        <div class="col-md-4 col-md-offset-4">-->
        <!--            <button type="submit" class="btn btn-danger btn-buscar">CERRADO</button>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
    </div>
</div>
@endsection
