@extends('layouts.app')

@section('content')
<div class="general-container">
    <div class="login bg-image">
        <div class="login-container">
            <div class="panel login-panel">
                <div class="panel-heading">
                    <h2 class="text-center">INTRANET</h2>
                    <div class="triang-abajo"></div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
    
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="username" type="text" placeholder="Usuario:" class="form-control text-center" name="username" value="{{ old('username') }}" required autofocus>
    
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-xs-12">
                                <input id="password" type="password" placeholder="Contraseña" class="form-control text-center" name="password" required>
    
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Recordarme
                                    </label>
                                </div>
                            </div>
                        </div>
    
                        <div class="form-group text-center">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-naranja">
                                    Login
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    He olvidado mi contraseña
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
