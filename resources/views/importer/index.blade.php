@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="padding-top: 120px;">
        <div class="col-md-10 col-md-offset-1">
            <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div id="headingOne" class="panel-heading" role="tab">
                        <a href="#collapseOne" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Clientes</h4>
                        </a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center" style="margin-top: 20px; margin-bottom: 20px;">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <label for="tipo-filtro-cliente">Filtro</label>
                                        <select id="tipo-filtro-cliente" class="form-control">
                                            <option value="-1">Seleccione una opción</option>
                                            <option value="1">Nro Cliente</option>
                                            <option value="2">DNI/RUC</option>
                                            <option value="3">Nombre y Apellidos</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="filtro-cliente" placeholder="Filtro" disabled>
                                    </div>
                                    <button id="filtrar-cliente" type="submit" class="btn btn-default" disabled>Buscar</button>
                                </form>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <table class="table table-responsive table-striped text-center">
                                    <thead>
                                        <tr>
                                            <th>-</th>
                                            <th>Nro Cliente</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>DNI</th>
                                            <th>RUC</th>
                                        </tr>
                                    </thead>
                                    <tbody id="customers-list">
                                    </tbody>
                                </table>
                                <div id="customer-spinner" class="spinner">
                                    <div class="dot1"></div>
                                    <div class="dot2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <form id="import-form" action="{{ url('import/users') }}" method="POST">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <button id="import" type="submit" class="btn btn-primary btn-large">Importar</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var customersList = null, employeesList = null;
            var customersToSaveList = [], employeesToSaveList = [];
            var customerTemplate = '<tr class="m-15-0">' + 
                            '<td><input type="checkbox" id="cb-__customer_id__" class="cb-customer m-0"/></td>' +
                            '<td class="customer-no">__customer_no__</td>' +
                            '<td class="first-name">__first_name__</td>' +
                            '<td class="last-name">__last_name__</td>' +
                            '<td class="dni">__dni__</td>' +
                            '<td class="ruc">__ruc__</td>' +
                        '</tr>';
            var employeeTemplate = '<tr class="m-15-0">' + 
                            '<td><input type="checkbox" id="cb-__employee_id__" class="cb-employee m-0"/></td>' +
                            '<td class="employee-code">__employee_code__</td>' +
                            '<td class="fullname" data-firstname="__first_name__" data-lastname="__last_name__">__fullname__</td>' +
                        '</tr>';

            function parseCustomers(customers, length, isRandom) {
                for(var i = 0; i < length; i++) {
                    var random = i;
                    if (isRandom) {
                        random = Math.floor(Math.random() * customers.length - i);
                    }
                    $("#customers-list").append(
                        customerTemplate.replace("__customer_id__", customers[random].$id)
                        .replace("__customer_no__", customers[random].CustomerNo)
                        .replace("__first_name__", customers[random].FirstName)
                        .replace("__last_name__", customers[random].LastName)
                        .replace("__dni__", customers[random].DNI)
                        .replace("__ruc__", customers[random].RUC)
                    );
                }
            }

            function parseEmployees(employees, length, isRandom) {
                for(var i = 0; i < length; i++) {
                    var random = i;
                    if (isRandom) {
                        random = Math.floor(Math.random() * employees.length - i);
                    }
                    $("#employees-list").append(
                        employeeTemplate.replace("__employee_id__", employees[random].$id)
                        .replace("__employee_code__", employees[random].EmployeeCode)
                        .replace("__first_name__", employees[random].FirstName)
                        .replace("__last_name__", employees[random].LastName)
                        .replace("__fullname__", employees[random].FullName)         
                    );
                }
            }

            $("#customers-list").on("change", ".cb-customer", function(e) {
                var $container = $(this).parents("tr");
                var customer = {
                    "customerNo": $container.find(".customer-no").text(),
                    "firstName": $container.find(".first-name").text(),
                    "lastName": $container.find(".last-name").text(),
                    "dni": $container.find(".dni").text(),
                    "ruc": $container.find(".ruc").text()
                };
                customersToSaveList.push(customer);
            });

            $("#employees-list").on("change", ".cb-employee", function(e) {
                if (this.checked) {
                    var $container = $(this).parents("tr");
                    var employee = {
                        "employeeCode": $container.find(".employee-code").text(),
                        "firstName": $container.find(".fullname").attr("data-firstname"),
                        "lastName": $container.find(".fullname").attr("data-lastname")
                    };
                    employeesToSaveList.push(employee);
                } else {
                    var employeeCode = $(this).parents("tr").find(".employee-code").text();
                    var employeeIndex = employeesToSaveList.findIndex(function (employee) {
                        return employee.employeeCode === employeeCode;
                    });
                    employeesToSaveList.splice(employeeIndex, 1);
                }
                
            });

            $("#tipo-filtro-cliente").change(function () {
                var option = Number($(this).val());
                if (option > 0 && option < 4) {
                    $("#filtro-cliente").attr("disabled", false);
                    $("#filtrar-cliente").attr("disabled", false);
                } else {
                    $("#filtro-cliente").attr("disabled", true);
                    $("#filtrar-cliente").attr("disabled", true);
                }
            });

            $("#tipo-filtro-empleado").change(function () {
                var option = Number($(this).val());
                if (option > 0 && option < 3) {
                    $("#filtro-empleado").attr("disabled", false);
                    $("#filtrar-empleado").attr("disabled", false);
                } else {
                    $("#filtro-empleado").attr("disabled", true);
                    $("#filtrar-empleado").attr("disabled", true);
                }
            });

            $("#filtrar-cliente").click(function (e) {
                e.preventDefault();
                var opcionFiltro = $("#tipo-filtro-cliente").val();
                var filtro = $("#filtro-cliente").val().trim();
                var result = null;
                if (opcionFiltro < 0 || opcionFiltro > 3 || filtro.length === 0) {
                    swal("Oops...", "Debes ingresar un filtro correcto.", "error");
                    return;
                }
                $("#customers-list").html("");
                $("#customer-spinner").fadeIn();
                switch (opcionFiltro) {
                    case "1":
                        result = customerList.filter(function (customer) {
                            return customer.CustomerNo.indexOf(filtro) >= 0;
                        });
                        break;
                    case "2":
                        result = customerList.filter(function (customer) {
                            return ((customer.DNI !== null) ? customer.DNI.indexOf(filtro) >= 0 : false) || 
                                ((customer.RUC !== null) ? customer.RUC.indexOf(filtro) >= 0 : false);
                        });
                        break;
                    case "3":
                        result = customerList.filter(function (customer) {
                            return (customer.FirstName + (customer.LastName !== null ? customer.LastName : "")).toUpperCase().indexOf(filtro.toUpperCase()) >= 0;
                        })
                        break;
                }
                $("#customer-spinner").fadeOut();
                if (result === null || result.length === 0) {
                    $("#customers-list").html("<p class='text-center'>No se encontraron resultados para el filtro de búsqueda</p>");
                } else {
                    parseCustomers(result, result.length, 0);
                }
            });

            $("#filtrar-empleado").click(function (e) {
                e.preventDefault();
                var opcionFiltro = $("#tipo-filtro-empleado").val();
                var filtro = $("#filtro-empleado").val().trim();
                var result = null;
                if (opcionFiltro < 0 || opcionFiltro > 2 || filtro.length === 0) {
                    swal("Oops...", "Debes ingresar un filtro correcto.", "error");
                    return;
                }
                $("#employees-list").html("");
                $("#employee-spinner").fadeIn();
                switch (opcionFiltro) {
                    case "1":
                        result = employeeList.filter(function (employee) {
                            return employee.EmployeeCode.toUpperCase().indexOf(filtro.toUpperCase()) >= 0;
                        });
                        break;
                    case "2":
                        result = employeeList.filter(function (employee) {
                            return employee.FullName.toUpperCase().indexOf(filtro.toUpperCase()) >= 0;
                        });
                        break;
                }
                $("#employee-spinner").fadeOut();
                if (result === null || result.length === 0) {
                    $("#employees-list").html("<p class='text-center'>No se encontraron resultados para el filtro de búsqueda</p>");
                } else {
                    parseEmployees(result, result.length, 0);
                }
            });

            $("#import-form").submit(function (e) {
                e.preventDefault();
                var customerLength = customersToSaveList.length;
                var employeeLength = employeesToSaveList.length;
                var url = $(this).attr("action");
                swal({
                    title: "¿Estás seguro?",
                    text: "Estás apunto de importar " + customerLength + " clientes y " + employeeLength + " trabajadores para que tengan acceso a la intranet.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sí, importar!",
                    cancelButtonText: "No, cancelar.",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.post(url, {
                            "_token": $("#token").val(),
                            "users": JSON.stringify({
                                "customers": customersToSaveList,
                                "employees": employeesToSaveList
                            })
                        }).done(function (response) {
                            console.log(response);
                            swal("Importado!", "Se ha importado satisfactoriamente los " + customerLength + " clientes y " + employeeLength + " trabajadores.", "success");
                        }).fail(function (error) {
                            console.log(error);
                            swal("Error", "Un error ha ocurrido, vuelva a intentar y si falla nuevamente, contacte a soporte.", "error");
                        })
                    } else {
                        swal("Cancelado", "Se ha cancelado la importación de clientes y trabajadores.", "error");
                    }
                })
            });

            $.getJSON("http://170.0.82.214/catalogos/api/customer")
                .done(function(customers) {            
                    customerList = customers;
                    $("#customer-spinner").fadeOut();
                    parseCustomers(customers, 10, true);
                }).fail(function(error) {
                    console.log(error);
                    $("#customer-spinner").fadeOut();
                });

            $.getJSON("http://170.0.82.214/catalogos/api/employee")
                .done(function(employees) {            
                    employeeList = employees;
                    $("#employee-spinner").fadeOut();
                    parseEmployees(employees, 10, true);
                }).fail(function(error) {
                    console.log(error);
                    $("#employee-spinner").fadeOut();
                });
            
        });
    </script>
@endsection