@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 100px;">
    <div class="breadcrumb">
        <a href="{{ url('/') }}"><< Volver</a>
    </div>
    <h2 class="text-center titulo">Consulta de Stock</h2>
    <div class="row">
        <div class="jumbotron">
            <form id="search-product"  class="jumbotron" style="padding: 0px 0px 30px">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                <div class="col-md-4 form-group">
                    <label for="product-code" class="text-uppercase">Código de producto:</label>
                    <input type="number" min="0" class="form-control" id="product-code" placeholder="Código de producto">
                </div>
                <!--<div class="col-md-5 form-group">-->
                <!--    <label for="store" class="text-uppercase">Selecciona tienda:</label>-->
                <!--    <div class="dropdown">-->
                <!--        <select id="store" class="form-control">-->
                <!--            <option value="-1">Seleccione tienda</option>-->
                <!--            <option value="5">Tienda Higuereta</option>-->
                <!--            <option value="2">Tienda Jr. Ica</option>-->
                <!--            <option value="0">Tienda Pueblo Libre</option>-->
                <!--            <option value="1">Tienda San Juan Lurigancho</option>-->
                <!--        </select>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="col-md-4" style="margin-top: 17px;">
                    <button type="submit" class="btn btn-primary btn-buscar">VER STOCK</button>  
                </div>
                <div class="col-md-4 hidden" style="margin-top: 17px;" id="btn-cart">
                    <a href="{{ url('orders/summary') }}" class="btn btn-success btn-buscar">IR AL CARRITO <span class="circle">{{ $cartTotalQty }}</span></a>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4" style="padding-left: 0;">
            <div class="jumbotron" style="padding: 30px 25px;">
                <div class="form-group">
                    <label for="nombre-producto">Producto</label>
                    <input id="nombre-producto" type="text" name="nombre-producto" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <label for="marca">Marca</label>
                    <input id="marca" type="text" name="marca" class="form-control" disabled/>
                </div>
                <div class="form-group">
                     <label for="categoria">Categoría</label>
                    <input id="categoria" type="text" name="categoria" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <label for="genero">Género</label>
                    <input id="genero" type="text" name="genero" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <label for="precio">Precio Producto</label>
                    <input id="precio" type="text" name="precio" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <label for="precio-asesora">Precio Asesora</label>
                    <input id="precio-asesora" type="text" name="precio-asesora" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <label for="descuento">Descuento</label>
                    <input id="descuento" type="text" name="descuento" class="form-control" disabled/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-buscar">VER GUÍA DE TALLAS</button>
                </div>
            </div>
        </div>
        <div class="col-md-8" style="padding-right: 0;">
            <div class="jumbotron" style="padding: 30px 25px;">
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>TALLA</th>
                            <th>STOCK</th>
                            <th>CANTIDAD AGREGADA</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody id="product-list">
                    </tbody>
                </table>
                <div class="spinner hidden">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript">
        var productListTemplate = "<tr data-style='__style_name__' data-sku='__sku__'>";
        productListTemplate += "<th>__size__</th>";
        productListTemplate += "<td>__stock__</td>";
        productListTemplate += "<td>__quantityProductAdd__</td>";
        productListTemplate += "<td>";
        productListTemplate += "<button class='btn btn-primary btn-md btn-add-to-cart' __disabled__>";
        productListTemplate += "<span class='glyphicon glyphicon-plus'></span>";
        productListTemplate += " Añadir al carrito";
        productListTemplate += "</button>";
        productListTemplate += "</td>";
        productListTemplate += "</tr>";
        
        var products = window.sessionStorage.getItem("cart");
        
        var loadPage = function () {
            $("#search-product").submit(searchProduct);
            $(document).on("click", ".btn-add-to-cart", addToCart);
            
            if (products == null) {
                products = [];
            } else {
                products = JSON.parse(products).products;
                
                if (products.length > {{ $cartTotalQty }}) {
                    var cart = {
                        customer: "{{Auth::user()->extid}}",
                        products: products
                    };
                    
                    saveSession(cart);
                }
            }
            
            updateCart(products);
        };
        
        if($('.circle').text() != '0') {
            $('#btn-cart').removeClass('hidden');
        }
        var saveSession = function (cart) {
            $.post("{{ url('orders/cart') }}", { 
                _token: $("#token").val(),
                cart: cart
            }).done(function (response) {
                console.log(response);
            }).fail(function (error) {
                console.log("error", error);
            });
        }
        
        var cartTotalQty = function (products) {
            return products.reduce(function (last, current) {
                return last + current.Qty;
            }, 0);
        };
        
        var updateCart = function (products) {
            var totalQty = cartTotalQty(products);
            $(".circle").text(totalQty);
        };

        var addToCart = function () {
            var $tr = $(this).parents("tr");
            var trQuantity = $(this).parent().prev();
            var validateStock = trQuantity.prev().text();
            var contador = Number(trQuantity.text());
            trQuantity.text(++contador);
            console.log(trQuantity.text());
            if(trQuantity.text() == validateStock) {
                $(this).attr("disabled",true);
            }
            
            var productToAdd = {
                StyleName: $tr.attr("data-style"),
                SKU: $tr.attr("data-sku"),
                SizeCode: $tr.find("th").text(),
                Stock: $tr.find("td").eq(0).text(),
                ProductDescription: $("#nombre-producto").val(),
                Brand: $("#marca").val(),
                Category: $("#categoria").val(),
                Gender: $("#genero").val(),
                RetailPrice: $("#precio").val(),
                ConsultantPrice: $("#precio-asesora").val(),
                Qty: 1
            };
            
            var foundProduct = products.find(function (product) {
                return product.SKU === productToAdd.SKU;
            });
            
            if (foundProduct) {
                foundProduct.Qty += 1;
            } else {
                products.push(productToAdd);
            }
            
            updateCart(products);
            
            var cart = {
                customer: "{{Auth::user()->extid}}",
                products: products
            };
            
            window.sessionStorage.setItem("cart", JSON.stringify(cart));
            
            saveSession(cart);
        };
        
        var searchProduct = function (e) {
            e.preventDefault();
            var productCode = $("#product-code").val();
            // var store = $("#store").val();
            var pattern = /^\d{6}$/;
            if (productCode.length === 0 || !pattern.test(productCode)) {
                swal("Oops...", "Debes ingresar un código de producto!", "error");
                return;
            }
            var $searchButton = $(this).find("button");
            $searchButton.attr("disabled", "true");
            $(".spinner").removeClass("hidden");
            $.getJSON("http://170.0.82.214/catalogos/api/product/" + productCode)
            .done(function (products) {
                $(".spinner").addClass("hidden");
                $("#btn-cart").removeClass("hidden");
                $("#product-list").html("");
                $searchButton.removeAttr("disabled");
                if (products == null) {
                    $("#product-list").html("<p>No se encontraron resultados.</p>");
                } else {
                    products.forEach(function (product) {
                        $("#product-list").append(
                            productListTemplate.replace("__style_name__", product.StyleName)
                                .replace("__sku__", product.SKU)
                                .replace("__size__", product.SizeCode)
                                .replace("__stock__", product.OnHandQty)
                                .replace("__quantityProductAdd__", 0)
                                .replace("__disabled__", product.OnHandQty > 0 ? "" : "disabled")
                        );
                    });
                    $("#nombre-producto").val(products[0].ProductDescription);
                    $("#marca").val(products[0].Brand);
                    $("#categoria").val(products[0].Category);
                    $("#genero").val(products[0].Gender);
                    $("#precio").val(products[0].RetailPrice);
                    $("#precio-asesora").val(products[0].ConsultantPrice == 0 ? "-" : products[0].ConsultantPrice);
                }
            })
            .fail(function (error) {
                console.log(error);
                $("#product-list").html("");
                $("#nombre-producto").val("");
                $("#marca").val("");
                $("#categoria").val("");
                $("#genero").val("");
                $("#precio").val("");
                $("#precio-asesora").val("");
            });
        };
        
        $(document).ready(loadPage);
    </script>
@endsection
