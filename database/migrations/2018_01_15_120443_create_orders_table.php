<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer');
            $table->dateTime('order_date');
            $table->string('external_id');
            $table->decimal('shipping', 11, 5);
            $table->decimal('subtotal', 11, 5);
            $table->decimal('tax', 11, 5);
            $table->decimal('total', 11, 5);
            $table->integer('items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
