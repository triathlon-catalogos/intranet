<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('name');
            $table->string('region_id');
            $table->string('province_id');
            $table->timestamps();
            
            $table->foreign('region_id')
                ->references('id')->on('regions')
                ->onDelete('cascade');
                
            
            $table->foreign('province_id')
                ->references('id')->on('provinces')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('districts', function (Blueprint $table) {
            $table->dropForeign(['region_id', 'province_id']);
        });
        Schema::dropIfExists('districts');
    }
}
