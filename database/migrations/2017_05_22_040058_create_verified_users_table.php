<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifiedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verified_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('catalog_id')->unsigned();
            $table->boolean('is_valid');
            $table->timestamps();
            
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
                
            
            $table->foreign('catalog_id')
                ->references('id')
                ->on('catalogs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('verified_users');
    }
}
