<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Support',
        	'lastname' => 'Admin',
        	'email' => 'admin@dev.ai',
        	'username' => 'admin',
        	'password' => bcrypt('secret'),
            'dni' => '11111111',
            'ruc' => '10111111111',
            'extid' => 'admin',
            'type' => 'employee'
        ]);
    }
}
