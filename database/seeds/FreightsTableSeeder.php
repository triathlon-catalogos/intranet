<?php

use Illuminate\Database\Seeder;

class FreightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('freights')->insert([
            [
            	'min_quantity' => 1,
            	'max_quantity' => 4,
            	'near_destination_price' => 15.00,
            	'far_destination_price' => 20.00,
        	],
        	[
            	'min_quantity' => 5,
            	'max_quantity' => 8,
            	'near_destination_price' => 25.00,
            	'far_destination_price' => 30.00,
        	],
        	[
            	'min_quantity' => 9,
            	'max_quantity' => 12,
            	'near_destination_price' => 35.00,
            	'far_destination_price' => 40.00,
        	],
        	[
            	'min_quantity' => 13,
            	'max_quantity' => 99,
            	'near_destination_price' => 45.00,
            	'far_destination_price' => 60.00,
        	],
        ]);
    }
}
