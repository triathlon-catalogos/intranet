<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'id' => '010100',
                'name' => 'Chachapoyas',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '010200',
                'name' => 'Bagua',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '010300',
                'name' => 'Bongará',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '010400',
                'name' => 'Condorcanqui',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '010500',
                'name' => 'Luya',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '010600',
                'name' => 'Rodríguez de Mendoza',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '010700',
                'name' => 'Utcubamba',
                'region_id' => '010000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '020100',
                'name' => 'Huaraz',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '020200',
                'name' => 'Aija',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '020300',
                'name' => 'Antonio Raymondi',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '020400',
                'name' => 'Asunción',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '020500',
                'name' => 'Bolognesi',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '020600',
                'name' => 'Carhuaz',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '020700',
                'name' => 'Carlos Fermín Fitzcarrald',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '020800',
                'name' => 'Casma',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '020900',
                'name' => 'Corongo',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '021000',
                'name' => 'Huari',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '021100',
                'name' => 'Huarmey',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '021200',
                'name' => 'Huaylas',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '021300',
                'name' => 'Mariscal Luzuriaga',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '021400',
                'name' => 'Ocros',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '021500',
                'name' => 'Pallasca',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '021600',
                'name' => 'Pomabamba',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '021700',
                'name' => 'Recuay',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '021800',
                'name' => 'Santa',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '021900',
                'name' => 'Sihuas',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '022000',
                'name' => 'Yungay',
                'region_id' => '020000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '030100',
                'name' => 'Abancay',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '030200',
                'name' => 'Andahuaylas',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '030300',
                'name' => 'Antabamba',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '030400',
                'name' => 'Aymaraes',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '030500',
                'name' => 'Cotabambas',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '030600',
                'name' => 'Chincheros',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '030700',
                'name' => 'Grau',
                'region_id' => '030000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '040100',
                'name' => 'Arequipa',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '040200',
                'name' => 'Camaná',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '040300',
                'name' => 'Caravelí',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '040400',
                'name' => 'Castilla',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '040500',
                'name' => 'Caylloma',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '040600',
                'name' => 'Condesuyos',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '040700',
                'name' => 'Islay',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '040800',
                'name' => 'La Uniòn',
                'region_id' => '040000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '050100',
                'name' => 'Huamanga',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '050200',
                'name' => 'Cangallo',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '050300',
                'name' => 'Huanca Sancos',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '050400',
                'name' => 'Huanta',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '050500',
                'name' => 'La Mar',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '050600',
                'name' => 'Lucanas',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '050700',
                'name' => 'Parinacochas',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '050800',
                'name' => 'Pàucar del Sara Sara',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '050900',
                'name' => 'Sucre',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '051000',
                'name' => 'Víctor Fajardo',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '051100',
                'name' => 'Vilcas Huamán',
                'region_id' => '050000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '060100',
                'name' => 'Cajamarca',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '060200',
                'name' => 'Cajabamba',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '060300',
                'name' => 'Celendín',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '060400',
                'name' => 'Chota',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '060500',
                'name' => 'Contumazá',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '060600',
                'name' => 'Cutervo',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '060700',
                'name' => 'Hualgayoc',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '060800',
                'name' => 'Jaén',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '060900',
                'name' => 'San Ignacio',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '061000',
                'name' => 'San Marcos',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '061100',
                'name' => 'San Miguel',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '061200',
                'name' => 'San Pablo',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '061300',
                'name' => 'Santa Cruz',
                'region_id' => '060000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '070100',
                'name' => 'Prov. Const. del Callao',
                'region_id' => '070000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '080100',
                'name' => 'Cusco',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '080200',
                'name' => 'Acomayo',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '080300',
                'name' => 'Anta',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '080400',
                'name' => 'Calca',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '080500',
                'name' => 'Canas',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '080600',
                'name' => 'Canchis',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '080700',
                'name' => 'Chumbivilcas',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '080800',
                'name' => 'Espinar',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '080900',
                'name' => 'La Convención',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '081000',
                'name' => 'Paruro',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '081100',
                'name' => 'Paucartambo',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '081200',
                'name' => 'Quispicanchi',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '081300',
                'name' => 'Urubamba',
                'region_id' => '080000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '090100',
                'name' => 'Huancavelica',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '090200',
                'name' => 'Acobamba',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '090300',
                'name' => 'Angaraes',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '090400',
                'name' => 'Castrovirreyna',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '090500',
                'name' => 'Churcampa',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '090600',
                'name' => 'Huaytará',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '090700',
                'name' => 'Tayacaja',
                'region_id' => '090000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '100100',
                'name' => 'Huánuco',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '100200',
                'name' => 'Ambo',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '100300',
                'name' => 'Dos de Mayo',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '100400',
                'name' => 'Huacaybamba',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '100500',
                'name' => 'Huamalíes',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '100600',
                'name' => 'Leoncio Prado',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '100700',
                'name' => 'Marañón',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '100800',
                'name' => 'Pachitea',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '100900',
                'name' => 'Puerto Inca',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '101000',
                'name' => 'Lauricocha ',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '101100',
                'name' => 'Yarowilca ',
                'region_id' => '100000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '110100',
                'name' => 'Ica ',
                'region_id' => '110000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '110200',
                'name' => 'Chincha ',
                'region_id' => '110000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '110300',
                'name' => 'Nasca ',
                'region_id' => '110000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '110400',
                'name' => 'Palpa ',
                'region_id' => '110000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '110500',
                'name' => 'Pisco ',
                'region_id' => '110000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '120100',
                'name' => 'Huancayo ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '120200',
                'name' => 'Concepción ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '120300',
                'name' => 'Chanchamayo ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '120400',
                'name' => 'Jauja ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '120500',
                'name' => 'Junín ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '120600',
                'name' => 'Satipo ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '120700',
                'name' => 'Tarma ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '120800',
                'name' => 'Yauli ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '120900',
                'name' => 'Chupaca ',
                'region_id' => '120000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '130100',
                'name' => 'Trujillo ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '130200',
                'name' => 'Ascope ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '130300',
                'name' => 'Bolívar ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '130400',
                'name' => 'Chepén ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '130500',
                'name' => 'Julcán ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '130600',
                'name' => 'Otuzco ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '130700',
                'name' => 'Pacasmayo ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '130800',
                'name' => 'Pataz ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '130900',
                'name' => 'Sánchez Carrión ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '131000',
                'name' => 'Santiago de Chuco ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '131100',
                'name' => 'Gran Chimú ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '131200',
                'name' => 'Virú ',
                'region_id' => '130000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '140100',
                'name' => 'Chiclayo ',
                'region_id' => '140000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '140200',
                'name' => 'Ferreñafe ',
                'region_id' => '140000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '140300',
                'name' => 'Lambayeque ',
                'region_id' => '140000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '150100',
                'name' => 'Lima ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '150200',
                'name' => 'Barranca ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '150300',
                'name' => 'Cajatambo ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '150400',
                'name' => 'Canta ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '150500',
                'name' => 'Cañete ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '150600',
                'name' => 'Huaral ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '150700',
                'name' => 'Huarochirí ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '150800',
                'name' => 'Huaura ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '150900',
                'name' => 'Oyón ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '151000',
                'name' => 'Yauyos ',
                'region_id' => '150000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '160100',
                'name' => 'Maynas ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '160200',
                'name' => 'Alto Amazonas ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '160300',
                'name' => 'Loreto ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '160400',
                'name' => 'Mariscal Ramón Castilla ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '160500',
                'name' => 'Requena ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '160600',
                'name' => 'Ucayali ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '160700',
                'name' => 'Datem del Marañón ',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '160800',
                'name' => 'Putumayo',
                'region_id' => '160000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '170100',
                'name' => 'Tambopata ',
                'region_id' => '170000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '170200',
                'name' => 'Manu ',
                'region_id' => '170000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '170300',
                'name' => 'Tahuamanu ',
                'region_id' => '170000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '180100',
                'name' => 'Mariscal Nieto ',
                'region_id' => '180000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '180200',
                'name' => 'General Sánchez Cerro ',
                'region_id' => '180000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '180300',
                'name' => 'Ilo ',
                'region_id' => '180000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '190100',
                'name' => 'Pasco ',
                'region_id' => '190000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '190200',
                'name' => 'Daniel Alcides Carrión ',
                'region_id' => '190000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '190300',
                'name' => 'Oxapampa ',
                'region_id' => '190000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '200100',
                'name' => 'Piura ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '200200',
                'name' => 'Ayabaca ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '200300',
                'name' => 'Huancabamba ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '200400',
                'name' => 'Morropón ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '200500',
                'name' => 'Paita ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '200600',
                'name' => 'Sullana ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '200700',
                'name' => 'Talara ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '200800',
                'name' => 'Sechura ',
                'region_id' => '200000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '210100',
                'name' => 'Puno ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '210200',
                'name' => 'Azángaro ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '210300',
                'name' => 'Carabaya ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '210400',
                'name' => 'Chucuito ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '210500',
                'name' => 'El Collao ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '210600',
                'name' => 'Huancané ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '210700',
                'name' => 'Lampa ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '210800',
                'name' => 'Melgar ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '210900',
                'name' => 'Moho ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '211000',
                'name' => 'San Antonio de Putina ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '211100',
                'name' => 'San Román ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '211200',
                'name' => 'Sandia ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '211300',
                'name' => 'Yunguyo ',
                'region_id' => '210000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '220100',
                'name' => 'Moyobamba ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '220200',
                'name' => 'Bellavista ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '220300',
                'name' => 'El Dorado ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '220400',
                'name' => 'Huallaga ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '220500',
                'name' => 'Lamas ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '220600',
                'name' => 'Mariscal Cáceres ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '220700',
                'name' => 'Picota ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '220800',
                'name' => 'Rioja ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '220900',
                'name' => 'San Martín ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '221000',
                'name' => 'Tocache ',
                'region_id' => '220000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '230100',
                'name' => 'Tacna ',
                'region_id' => '230000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '230200',
                'name' => 'Candarave ',
                'region_id' => '230000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '230300',
                'name' => 'Jorge Basadre ',
                'region_id' => '230000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '230400',
                'name' => 'Tarata ',
                'region_id' => '230000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '240100',
                'name' => 'Tumbes ',
                'region_id' => '240000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '240200',
                'name' => 'Contralmirante Villar ',
                'region_id' => '240000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '240300',
                'name' => 'Zarumilla ',
                'region_id' => '240000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '250100',
                'name' => 'Coronel Portillo ',
                'region_id' => '250000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '250200',
                'name' => 'Atalaya ',
                'region_id' => '250000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '250300',
                'name' => 'Padre Abad ',
                'region_id' => '250000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '250400',
                'name' => 'Purús',
                'region_id' => '250000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}