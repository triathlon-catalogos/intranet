<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('districts')->delete();
        
        \DB::table('districts')->insert(array (
            0 => 
            array (
                'id' => '010101',
                'name' => 'Chachapoyas',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '010102',
                'name' => 'Asunción',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '010103',
                'name' => 'Balsas',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '010104',
                'name' => 'Cheto',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '010105',
                'name' => 'Chiliquin',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '010106',
                'name' => 'Chuquibamba',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '010107',
                'name' => 'Granada',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '010108',
                'name' => 'Huancas',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '010109',
                'name' => 'La Jalca',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '010110',
                'name' => 'Leimebamba',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '010111',
                'name' => 'Levanto',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '010112',
                'name' => 'Magdalena',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '010113',
                'name' => 'Mariscal Castilla',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '010114',
                'name' => 'Molinopampa',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '010115',
                'name' => 'Montevideo',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '010116',
                'name' => 'Olleros',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '010117',
                'name' => 'Quinjalca',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '010118',
                'name' => 'San Francisco de Daguas',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '010119',
                'name' => 'San Isidro de Maino',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '010120',
                'name' => 'Soloco',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '010121',
                'name' => 'Sonche',
                'region_id' => '010000',
                'province_id' => '010100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '010201',
                'name' => 'Bagua',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '010202',
                'name' => 'Aramango',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '010203',
                'name' => 'Copallin',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '010204',
                'name' => 'El Parco',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '010205',
                'name' => 'Imaza',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '010206',
                'name' => 'La Peca',
                'region_id' => '010000',
                'province_id' => '010200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '010301',
                'name' => 'Jumbilla',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '010302',
                'name' => 'Chisquilla',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '010303',
                'name' => 'Churuja',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '010304',
                'name' => 'Corosha',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '010305',
                'name' => 'Cuispes',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '010306',
                'name' => 'Florida',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '010307',
                'name' => 'Jazan',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '010308',
                'name' => 'Recta',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '010309',
                'name' => 'San Carlos',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '010310',
                'name' => 'Shipasbamba',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '010311',
                'name' => 'Valera',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '010312',
                'name' => 'Yambrasbamba',
                'region_id' => '010000',
                'province_id' => '010300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '010401',
                'name' => 'Nieva',
                'region_id' => '010000',
                'province_id' => '010400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '010402',
                'name' => 'El Cenepa',
                'region_id' => '010000',
                'province_id' => '010400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '010403',
                'name' => 'Río Santiago',
                'region_id' => '010000',
                'province_id' => '010400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '010501',
                'name' => 'Lamud',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '010502',
                'name' => 'Camporredondo',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '010503',
                'name' => 'Cocabamba',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '010504',
                'name' => 'Colcamar',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '010505',
                'name' => 'Conila',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '010506',
                'name' => 'Inguilpata',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '010507',
                'name' => 'Longuita',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '010508',
                'name' => 'Lonya Chico',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '010509',
                'name' => 'Luya',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '010510',
                'name' => 'Luya Viejo',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '010511',
                'name' => 'María',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '010512',
                'name' => 'Ocalli',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '010513',
                'name' => 'Ocumal',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '010514',
                'name' => 'Pisuquia',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '010515',
                'name' => 'Providencia',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '010516',
                'name' => 'San Cristóbal',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '010517',
                'name' => 'San Francisco de Yeso',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '010518',
                'name' => 'San Jerónimo',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '010519',
                'name' => 'San Juan de Lopecancha',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '010520',
                'name' => 'Santa Catalina',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '010521',
                'name' => 'Santo Tomas',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '010522',
                'name' => 'Tingo',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '010523',
                'name' => 'Trita',
                'region_id' => '010000',
                'province_id' => '010500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '010601',
                'name' => 'San Nicolás',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '010602',
                'name' => 'Chirimoto',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '010603',
                'name' => 'Cochamal',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '010604',
                'name' => 'Huambo',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '010605',
                'name' => 'Limabamba',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '010606',
                'name' => 'Longar',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '010607',
                'name' => 'Mariscal Benavides',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '010608',
                'name' => 'Milpuc',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '010609',
                'name' => 'Omia',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '010610',
                'name' => 'Santa Rosa',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '010611',
                'name' => 'Totora',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '010612',
                'name' => 'Vista Alegre',
                'region_id' => '010000',
                'province_id' => '010600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '010701',
                'name' => 'Bagua Grande',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '010702',
                'name' => 'Cajaruro',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '010703',
                'name' => 'Cumba',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '010704',
                'name' => 'El Milagro',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '010705',
                'name' => 'Jamalca',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '010706',
                'name' => 'Lonya Grande',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '010707',
                'name' => 'Yamon',
                'region_id' => '010000',
                'province_id' => '010700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '020101',
                'name' => 'Huaraz',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '020102',
                'name' => 'Cochabamba',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '020103',
                'name' => 'Colcabamba',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '020104',
                'name' => 'Huanchay',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '020105',
                'name' => 'Independencia',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '020106',
                'name' => 'Jangas',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '020107',
                'name' => 'La Libertad',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '020108',
                'name' => 'Olleros',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '020109',
                'name' => 'Pampas Grande',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '020110',
                'name' => 'Pariacoto',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '020111',
                'name' => 'Pira',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '020112',
                'name' => 'Tarica',
                'region_id' => '020000',
                'province_id' => '020100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '020201',
                'name' => 'Aija',
                'region_id' => '020000',
                'province_id' => '020200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '020202',
                'name' => 'Coris',
                'region_id' => '020000',
                'province_id' => '020200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '020203',
                'name' => 'Huacllan',
                'region_id' => '020000',
                'province_id' => '020200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '020204',
                'name' => 'La Merced',
                'region_id' => '020000',
                'province_id' => '020200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '020205',
                'name' => 'Succha',
                'region_id' => '020000',
                'province_id' => '020200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '020301',
                'name' => 'Llamellin',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '020302',
                'name' => 'Aczo',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '020303',
                'name' => 'Chaccho',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '020304',
                'name' => 'Chingas',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '020305',
                'name' => 'Mirgas',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '020306',
                'name' => 'San Juan de Rontoy',
                'region_id' => '020000',
                'province_id' => '020300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '020401',
                'name' => 'Chacas',
                'region_id' => '020000',
                'province_id' => '020400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '020402',
                'name' => 'Acochaca',
                'region_id' => '020000',
                'province_id' => '020400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '020501',
                'name' => 'Chiquian',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '020502',
                'name' => 'Abelardo Pardo Lezameta',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '020503',
                'name' => 'Antonio Raymondi',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '020504',
                'name' => 'Aquia',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '020505',
                'name' => 'Cajacay',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '020506',
                'name' => 'Canis',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '020507',
                'name' => 'Colquioc',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '020508',
                'name' => 'Huallanca',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '020509',
                'name' => 'Huasta',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '020510',
                'name' => 'Huayllacayan',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '020511',
                'name' => 'La Primavera',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '020512',
                'name' => 'Mangas',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '020513',
                'name' => 'Pacllon',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '020514',
                'name' => 'San Miguel de Corpanqui',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '020515',
                'name' => 'Ticllos',
                'region_id' => '020000',
                'province_id' => '020500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '020601',
                'name' => 'Carhuaz',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '020602',
                'name' => 'Acopampa',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '020603',
                'name' => 'Amashca',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '020604',
                'name' => 'Anta',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '020605',
                'name' => 'Ataquero',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '020606',
                'name' => 'Marcara',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '020607',
                'name' => 'Pariahuanca',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '020608',
                'name' => 'San Miguel de Aco',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '020609',
                'name' => 'Shilla',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '020610',
                'name' => 'Tinco',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '020611',
                'name' => 'Yungar',
                'region_id' => '020000',
                'province_id' => '020600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '020701',
                'name' => 'San Luis',
                'region_id' => '020000',
                'province_id' => '020700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '020702',
                'name' => 'San Nicolás',
                'region_id' => '020000',
                'province_id' => '020700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '020703',
                'name' => 'Yauya',
                'region_id' => '020000',
                'province_id' => '020700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '020801',
                'name' => 'Casma',
                'region_id' => '020000',
                'province_id' => '020800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '020802',
                'name' => 'Buena Vista Alta',
                'region_id' => '020000',
                'province_id' => '020800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '020803',
                'name' => 'Comandante Noel',
                'region_id' => '020000',
                'province_id' => '020800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '020804',
                'name' => 'Yautan',
                'region_id' => '020000',
                'province_id' => '020800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '020901',
                'name' => 'Corongo',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '020902',
                'name' => 'Aco',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '020903',
                'name' => 'Bambas',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '020904',
                'name' => 'Cusca',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '020905',
                'name' => 'La Pampa',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '020906',
                'name' => 'Yanac',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '020907',
                'name' => 'Yupan',
                'region_id' => '020000',
                'province_id' => '020900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '021001',
                'name' => 'Huari',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '021002',
                'name' => 'Anra',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '021003',
                'name' => 'Cajay',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '021004',
                'name' => 'Chavin de Huantar',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '021005',
                'name' => 'Huacachi',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '021006',
                'name' => 'Huacchis',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '021007',
                'name' => 'Huachis',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '021008',
                'name' => 'Huantar',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '021009',
                'name' => 'Masin',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '021010',
                'name' => 'Paucas',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '021011',
                'name' => 'Ponto',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '021012',
                'name' => 'Rahuapampa',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '021013',
                'name' => 'Rapayan',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '021014',
                'name' => 'San Marcos',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '021015',
                'name' => 'San Pedro de Chana',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '021016',
                'name' => 'Uco',
                'region_id' => '020000',
                'province_id' => '021000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '021101',
                'name' => 'Huarmey',
                'region_id' => '020000',
                'province_id' => '021100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '021102',
                'name' => 'Cochapeti',
                'region_id' => '020000',
                'province_id' => '021100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '021103',
                'name' => 'Culebras',
                'region_id' => '020000',
                'province_id' => '021100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '021104',
                'name' => 'Huayan',
                'region_id' => '020000',
                'province_id' => '021100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '021105',
                'name' => 'Malvas',
                'region_id' => '020000',
                'province_id' => '021100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '021201',
                'name' => 'Caraz',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '021202',
                'name' => 'Huallanca',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '021203',
                'name' => 'Huata',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '021204',
                'name' => 'Huaylas',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '021205',
                'name' => 'Mato',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '021206',
                'name' => 'Pamparomas',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '021207',
                'name' => 'Pueblo Libre',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '021208',
                'name' => 'Santa Cruz',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '021209',
                'name' => 'Santo Toribio',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '021210',
                'name' => 'Yuracmarca',
                'region_id' => '020000',
                'province_id' => '021200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '021301',
                'name' => 'Piscobamba',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '021302',
                'name' => 'Casca',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '021303',
                'name' => 'Eleazar Guzmán Barron',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '021304',
                'name' => 'Fidel Olivas Escudero',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '021305',
                'name' => 'Llama',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '021306',
                'name' => 'Llumpa',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '021307',
                'name' => 'Lucma',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '021308',
                'name' => 'Musga',
                'region_id' => '020000',
                'province_id' => '021300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '021401',
                'name' => 'Ocros',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '021402',
                'name' => 'Acas',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '021403',
                'name' => 'Cajamarquilla',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '021404',
                'name' => 'Carhuapampa',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '021405',
                'name' => 'Cochas',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '021406',
                'name' => 'Congas',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '021407',
                'name' => 'Llipa',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '021408',
                'name' => 'San Cristóbal de Rajan',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => '021409',
                'name' => 'San Pedro',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => '021410',
                'name' => 'Santiago de Chilcas',
                'region_id' => '020000',
                'province_id' => '021400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => '021501',
                'name' => 'Cabana',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => '021502',
                'name' => 'Bolognesi',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => '021503',
                'name' => 'Conchucos',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => '021504',
                'name' => 'Huacaschuque',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => '021505',
                'name' => 'Huandoval',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => '021506',
                'name' => 'Lacabamba',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => '021507',
                'name' => 'Llapo',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => '021508',
                'name' => 'Pallasca',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => '021509',
                'name' => 'Pampas',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => '021510',
                'name' => 'Santa Rosa',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => '021511',
                'name' => 'Tauca',
                'region_id' => '020000',
                'province_id' => '021500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => '021601',
                'name' => 'Pomabamba',
                'region_id' => '020000',
                'province_id' => '021600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => '021602',
                'name' => 'Huayllan',
                'region_id' => '020000',
                'province_id' => '021600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => '021603',
                'name' => 'Parobamba',
                'region_id' => '020000',
                'province_id' => '021600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => '021604',
                'name' => 'Quinuabamba',
                'region_id' => '020000',
                'province_id' => '021600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => '021701',
                'name' => 'Recuay',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => '021702',
                'name' => 'Catac',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => '021703',
                'name' => 'Cotaparaco',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => '021704',
                'name' => 'Huayllapampa',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => '021705',
                'name' => 'Llacllin',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => '021706',
                'name' => 'Marca',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => '021707',
                'name' => 'Pampas Chico',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => '021708',
                'name' => 'Pararin',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => '021709',
                'name' => 'Tapacocha',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => '021710',
                'name' => 'Ticapampa',
                'region_id' => '020000',
                'province_id' => '021700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => '021801',
                'name' => 'Chimbote',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => '021802',
                'name' => 'Cáceres del Perú',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => '021803',
                'name' => 'Coishco',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => '021804',
                'name' => 'Macate',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => '021805',
                'name' => 'Moro',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => '021806',
                'name' => 'Nepeña',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => '021807',
                'name' => 'Samanco',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => '021808',
                'name' => 'Santa',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => '021809',
                'name' => 'Nuevo Chimbote',
                'region_id' => '020000',
                'province_id' => '021800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => '021901',
                'name' => 'Sihuas',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => '021902',
                'name' => 'Acobamba',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => '021903',
                'name' => 'Alfonso Ugarte',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => '021904',
                'name' => 'Cashapampa',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => '021905',
                'name' => 'Chingalpo',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => '021906',
                'name' => 'Huayllabamba',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => '021907',
                'name' => 'Quiches',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => '021908',
                'name' => 'Ragash',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => '021909',
                'name' => 'San Juan',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => '021910',
                'name' => 'Sicsibamba',
                'region_id' => '020000',
                'province_id' => '021900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => '022001',
                'name' => 'Yungay',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => '022002',
                'name' => 'Cascapara',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => '022003',
                'name' => 'Mancos',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => '022004',
                'name' => 'Matacoto',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => '022005',
                'name' => 'Quillo',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => '022006',
                'name' => 'Ranrahirca',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => '022007',
                'name' => 'Shupluy',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => '022008',
                'name' => 'Yanama',
                'region_id' => '020000',
                'province_id' => '022000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => '030101',
                'name' => 'Abancay',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => '030102',
                'name' => 'Chacoche',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => '030103',
                'name' => 'Circa',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => '030104',
                'name' => 'Curahuasi',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => '030105',
                'name' => 'Huanipaca',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => '030106',
                'name' => 'Lambrama',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => '030107',
                'name' => 'Pichirhua',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => '030108',
                'name' => 'San Pedro de Cachora',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => '030109',
                'name' => 'Tamburco',
                'region_id' => '030000',
                'province_id' => '030100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => '030201',
                'name' => 'Andahuaylas',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => '030202',
                'name' => 'Andarapa',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => '030203',
                'name' => 'Chiara',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => '030204',
                'name' => 'Huancarama',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => '030205',
                'name' => 'Huancaray',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => '030206',
                'name' => 'Huayana',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => '030207',
                'name' => 'Kishuara',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => '030208',
                'name' => 'Pacobamba',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => '030209',
                'name' => 'Pacucha',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => '030210',
                'name' => 'Pampachiri',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => '030211',
                'name' => 'Pomacocha',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => '030212',
                'name' => 'San Antonio de Cachi',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => '030213',
                'name' => 'San Jerónimo',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => '030214',
                'name' => 'San Miguel de Chaccrampa',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => '030215',
                'name' => 'Santa María de Chicmo',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => '030216',
                'name' => 'Talavera',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => '030217',
                'name' => 'Tumay Huaraca',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => '030218',
                'name' => 'Turpo',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => '030219',
                'name' => 'Kaquiabamba',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => '030220',
                'name' => 'José María Arguedas',
                'region_id' => '030000',
                'province_id' => '030200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => '030301',
                'name' => 'Antabamba',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => '030302',
                'name' => 'El Oro',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => '030303',
                'name' => 'Huaquirca',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => '030304',
                'name' => 'Juan Espinoza Medrano',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => '030305',
                'name' => 'Oropesa',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => '030306',
                'name' => 'Pachaconas',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => '030307',
                'name' => 'Sabaino',
                'region_id' => '030000',
                'province_id' => '030300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => '030401',
                'name' => 'Chalhuanca',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => '030402',
                'name' => 'Capaya',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => '030403',
                'name' => 'Caraybamba',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => '030404',
                'name' => 'Chapimarca',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => '030405',
                'name' => 'Colcabamba',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => '030406',
                'name' => 'Cotaruse',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => '030407',
                'name' => 'Ihuayllo',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => '030408',
                'name' => 'Justo Apu Sahuaraura',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => '030409',
                'name' => 'Lucre',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => '030410',
                'name' => 'Pocohuanca',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => '030411',
                'name' => 'San Juan de Chacña',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => '030412',
                'name' => 'Sañayca',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => '030413',
                'name' => 'Soraya',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => '030414',
                'name' => 'Tapairihua',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => '030415',
                'name' => 'Tintay',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => '030416',
                'name' => 'Toraya',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => '030417',
                'name' => 'Yanaca',
                'region_id' => '030000',
                'province_id' => '030400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => '030501',
                'name' => 'Tambobamba',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => '030502',
                'name' => 'Cotabambas',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => '030503',
                'name' => 'Coyllurqui',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => '030504',
                'name' => 'Haquira',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => '030505',
                'name' => 'Mara',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => '030506',
                'name' => 'Challhuahuacho',
                'region_id' => '030000',
                'province_id' => '030500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => '030601',
                'name' => 'Chincheros',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => '030602',
                'name' => 'Anco_Huallo',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => '030603',
                'name' => 'Cocharcas',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => '030604',
                'name' => 'Huaccana',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => '030605',
                'name' => 'Ocobamba',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => '030606',
                'name' => 'Ongoy',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => '030607',
                'name' => 'Uranmarca',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => '030608',
                'name' => 'Ranracancha',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => '030609',
                'name' => 'Rocchacc',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => '030610',
                'name' => 'El Porvenir',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => '030611',
                'name' => 'Los Chankas',
                'region_id' => '030000',
                'province_id' => '030600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => '030701',
                'name' => 'Chuquibambilla',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => '030702',
                'name' => 'Curpahuasi',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => '030703',
                'name' => 'Gamarra',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => '030704',
                'name' => 'Huayllati',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => '030705',
                'name' => 'Mamara',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => '030706',
                'name' => 'Micaela Bastidas',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => '030707',
                'name' => 'Pataypampa',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => '030708',
                'name' => 'Progreso',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => '030709',
                'name' => 'San Antonio',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => '030710',
                'name' => 'Santa Rosa',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => '030711',
                'name' => 'Turpay',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => '030712',
                'name' => 'Vilcabamba',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => '030713',
                'name' => 'Virundo',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => '030714',
                'name' => 'Curasco',
                'region_id' => '030000',
                'province_id' => '030700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => '040101',
                'name' => 'Arequipa',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => '040102',
                'name' => 'Alto Selva Alegre',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => '040103',
                'name' => 'Cayma',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => '040104',
                'name' => 'Cerro Colorado',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => '040105',
                'name' => 'Characato',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => '040106',
                'name' => 'Chiguata',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => '040107',
                'name' => 'Jacobo Hunter',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => '040108',
                'name' => 'La Joya',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => '040109',
                'name' => 'Mariano Melgar',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => '040110',
                'name' => 'Miraflores',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => '040111',
                'name' => 'Mollebaya',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => '040112',
                'name' => 'Paucarpata',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => '040113',
                'name' => 'Pocsi',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => '040114',
                'name' => 'Polobaya',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => '040115',
                'name' => 'Quequeña',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => '040116',
                'name' => 'Sabandia',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => '040117',
                'name' => 'Sachaca',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => '040118',
                'name' => 'San Juan de Siguas',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => '040119',
                'name' => 'San Juan de Tarucani',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => '040120',
                'name' => 'Santa Isabel de Siguas',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => '040121',
                'name' => 'Santa Rita de Siguas',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => '040122',
                'name' => 'Socabaya',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => '040123',
                'name' => 'Tiabaya',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => '040124',
                'name' => 'Uchumayo',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => '040125',
                'name' => 'Vitor',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => '040126',
                'name' => 'Yanahuara',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => '040127',
                'name' => 'Yarabamba',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => '040128',
                'name' => 'Yura',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => '040129',
                'name' => 'José Luis Bustamante Y Rivero',
                'region_id' => '040000',
                'province_id' => '040100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => '040201',
                'name' => 'Camaná',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => '040202',
                'name' => 'José María Quimper',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => '040203',
                'name' => 'Mariano Nicolás Valcárcel',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => '040204',
                'name' => 'Mariscal Cáceres',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => '040205',
                'name' => 'Nicolás de Pierola',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => '040206',
                'name' => 'Ocoña',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => '040207',
                'name' => 'Quilca',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => '040208',
                'name' => 'Samuel Pastor',
                'region_id' => '040000',
                'province_id' => '040200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => '040301',
                'name' => 'Caravelí',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => '040302',
                'name' => 'Acarí',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => '040303',
                'name' => 'Atico',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => '040304',
                'name' => 'Atiquipa',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => '040305',
                'name' => 'Bella Unión',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => '040306',
                'name' => 'Cahuacho',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => '040307',
                'name' => 'Chala',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => '040308',
                'name' => 'Chaparra',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => '040309',
                'name' => 'Huanuhuanu',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => '040310',
                'name' => 'Jaqui',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => '040311',
                'name' => 'Lomas',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => '040312',
                'name' => 'Quicacha',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => '040313',
                'name' => 'Yauca',
                'region_id' => '040000',
                'province_id' => '040300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => '040401',
                'name' => 'Aplao',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => '040402',
                'name' => 'Andagua',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => '040403',
                'name' => 'Ayo',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => '040404',
                'name' => 'Chachas',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => '040405',
                'name' => 'Chilcaymarca',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => '040406',
                'name' => 'Choco',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => '040407',
                'name' => 'Huancarqui',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => '040408',
                'name' => 'Machaguay',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => '040409',
                'name' => 'Orcopampa',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => '040410',
                'name' => 'Pampacolca',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => '040411',
                'name' => 'Tipan',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => '040412',
                'name' => 'Uñon',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => '040413',
                'name' => 'Uraca',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => '040414',
                'name' => 'Viraco',
                'region_id' => '040000',
                'province_id' => '040400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => '040501',
                'name' => 'Chivay',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => '040502',
                'name' => 'Achoma',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => '040503',
                'name' => 'Cabanaconde',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => '040504',
                'name' => 'Callalli',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => '040505',
                'name' => 'Caylloma',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => '040506',
                'name' => 'Coporaque',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => '040507',
                'name' => 'Huambo',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => '040508',
                'name' => 'Huanca',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => '040509',
                'name' => 'Ichupampa',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => '040510',
                'name' => 'Lari',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => '040511',
                'name' => 'Lluta',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => '040512',
                'name' => 'Maca',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => '040513',
                'name' => 'Madrigal',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => '040514',
                'name' => 'San Antonio de Chuca',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => '040515',
                'name' => 'Sibayo',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => '040516',
                'name' => 'Tapay',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => '040517',
                'name' => 'Tisco',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => '040518',
                'name' => 'Tuti',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => '040519',
                'name' => 'Yanque',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => '040520',
                'name' => 'Majes',
                'region_id' => '040000',
                'province_id' => '040500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => '040601',
                'name' => 'Chuquibamba',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => '040602',
                'name' => 'Andaray',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => '040603',
                'name' => 'Cayarani',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => '040604',
                'name' => 'Chichas',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => '040605',
                'name' => 'Iray',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => '040606',
                'name' => 'Río Grande',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => '040607',
                'name' => 'Salamanca',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => '040608',
                'name' => 'Yanaquihua',
                'region_id' => '040000',
                'province_id' => '040600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => '040701',
                'name' => 'Mollendo',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => '040702',
                'name' => 'Cocachacra',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => '040703',
                'name' => 'Dean Valdivia',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => '040704',
                'name' => 'Islay',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => '040705',
                'name' => 'Mejia',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => '040706',
                'name' => 'Punta de Bombón',
                'region_id' => '040000',
                'province_id' => '040700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => '040801',
                'name' => 'Cotahuasi',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => '040802',
                'name' => 'Alca',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => '040803',
                'name' => 'Charcana',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => '040804',
                'name' => 'Huaynacotas',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => '040805',
                'name' => 'Pampamarca',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => '040806',
                'name' => 'Puyca',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => '040807',
                'name' => 'Quechualla',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => '040808',
                'name' => 'Sayla',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => '040809',
                'name' => 'Tauria',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => '040810',
                'name' => 'Tomepampa',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => '040811',
                'name' => 'Toro',
                'region_id' => '040000',
                'province_id' => '040800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => '050101',
                'name' => 'Ayacucho',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => '050102',
                'name' => 'Acocro',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => '050103',
                'name' => 'Acos Vinchos',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => '050104',
                'name' => 'Carmen Alto',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => '050105',
                'name' => 'Chiara',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => '050106',
                'name' => 'Ocros',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => '050107',
                'name' => 'Pacaycasa',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => '050108',
                'name' => 'Quinua',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => '050109',
                'name' => 'San José de Ticllas',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => '050110',
                'name' => 'San Juan Bautista',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => '050111',
                'name' => 'Santiago de Pischa',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => '050112',
                'name' => 'Socos',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => '050113',
                'name' => 'Tambillo',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => '050114',
                'name' => 'Vinchos',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => '050115',
                'name' => 'Jesús Nazareno',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => '050116',
                'name' => 'Andrés Avelino Cáceres Dorregaray',
                'region_id' => '050000',
                'province_id' => '050100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => '050201',
                'name' => 'Cangallo',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => '050202',
                'name' => 'Chuschi',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => '050203',
                'name' => 'Los Morochucos',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => '050204',
                'name' => 'María Parado de Bellido',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => '050205',
                'name' => 'Paras',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => '050206',
                'name' => 'Totos',
                'region_id' => '050000',
                'province_id' => '050200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => '050301',
                'name' => 'Sancos',
                'region_id' => '050000',
                'province_id' => '050300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => '050302',
                'name' => 'Carapo',
                'region_id' => '050000',
                'province_id' => '050300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => '050303',
                'name' => 'Sacsamarca',
                'region_id' => '050000',
                'province_id' => '050300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => '050304',
                'name' => 'Santiago de Lucanamarca',
                'region_id' => '050000',
                'province_id' => '050300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => '050401',
                'name' => 'Huanta',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => '050402',
                'name' => 'Ayahuanco',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => '050403',
                'name' => 'Huamanguilla',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => '050404',
                'name' => 'Iguain',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => '050405',
                'name' => 'Luricocha',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => '050406',
                'name' => 'Santillana',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => '050407',
                'name' => 'Sivia',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => '050408',
                'name' => 'Llochegua',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => '050409',
                'name' => 'Canayre',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => '050410',
                'name' => 'Uchuraccay',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => '050411',
                'name' => 'Pucacolpa',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => '050412',
                'name' => 'Chaca',
                'region_id' => '050000',
                'province_id' => '050400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => '050501',
                'name' => 'San Miguel',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => '050502',
                'name' => 'Anco',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => '050503',
                'name' => 'Ayna',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => '050504',
                'name' => 'Chilcas',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => '050505',
                'name' => 'Chungui',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => '050506',
                'name' => 'Luis Carranza',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => '050507',
                'name' => 'Santa Rosa',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => '050508',
                'name' => 'Tambo',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => '050509',
                'name' => 'Samugari',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => '050510',
                'name' => 'Anchihuay',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => '050511',
                'name' => 'Oronccoy',
                'region_id' => '050000',
                'province_id' => '050500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => '050601',
                'name' => 'Puquio',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => '050602',
                'name' => 'Aucara',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => '050603',
                'name' => 'Cabana',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => '050604',
                'name' => 'Carmen Salcedo',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => '050605',
                'name' => 'Chaviña',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => '050606',
                'name' => 'Chipao',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => '050607',
                'name' => 'Huac-Huas',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => '050608',
                'name' => 'Laramate',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('districts')->insert(array (
            0 => 
            array (
                'id' => '050609',
                'name' => 'Leoncio Prado',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '050610',
                'name' => 'Llauta',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '050611',
                'name' => 'Lucanas',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '050612',
                'name' => 'Ocaña',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '050613',
                'name' => 'Otoca',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '050614',
                'name' => 'Saisa',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '050615',
                'name' => 'San Cristóbal',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '050616',
                'name' => 'San Juan',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '050617',
                'name' => 'San Pedro',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '050618',
                'name' => 'San Pedro de Palco',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '050619',
                'name' => 'Sancos',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '050620',
                'name' => 'Santa Ana de Huaycahuacho',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '050621',
                'name' => 'Santa Lucia',
                'region_id' => '050000',
                'province_id' => '050600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '050701',
                'name' => 'Coracora',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '050702',
                'name' => 'Chumpi',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '050703',
                'name' => 'Coronel Castañeda',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '050704',
                'name' => 'Pacapausa',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '050705',
                'name' => 'Pullo',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '050706',
                'name' => 'Puyusca',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '050707',
                'name' => 'San Francisco de Ravacayco',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '050708',
                'name' => 'Upahuacho',
                'region_id' => '050000',
                'province_id' => '050700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '050801',
                'name' => 'Pausa',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '050802',
                'name' => 'Colta',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '050803',
                'name' => 'Corculla',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '050804',
                'name' => 'Lampa',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '050805',
                'name' => 'Marcabamba',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '050806',
                'name' => 'Oyolo',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '050807',
                'name' => 'Pararca',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '050808',
                'name' => 'San Javier de Alpabamba',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '050809',
                'name' => 'San José de Ushua',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '050810',
                'name' => 'Sara Sara',
                'region_id' => '050000',
                'province_id' => '050800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '050901',
                'name' => 'Querobamba',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '050902',
                'name' => 'Belén',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '050903',
                'name' => 'Chalcos',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '050904',
                'name' => 'Chilcayoc',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '050905',
                'name' => 'Huacaña',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '050906',
                'name' => 'Morcolla',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '050907',
                'name' => 'Paico',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '050908',
                'name' => 'San Pedro de Larcay',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '050909',
                'name' => 'San Salvador de Quije',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '050910',
                'name' => 'Santiago de Paucaray',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '050911',
                'name' => 'Soras',
                'region_id' => '050000',
                'province_id' => '050900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '051001',
                'name' => 'Huancapi',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '051002',
                'name' => 'Alcamenca',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '051003',
                'name' => 'Apongo',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '051004',
                'name' => 'Asquipata',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '051005',
                'name' => 'Canaria',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '051006',
                'name' => 'Cayara',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '051007',
                'name' => 'Colca',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '051008',
                'name' => 'Huamanquiquia',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '051009',
                'name' => 'Huancaraylla',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '051010',
                'name' => 'Huaya',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '051011',
                'name' => 'Sarhua',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '051012',
                'name' => 'Vilcanchos',
                'region_id' => '050000',
                'province_id' => '051000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '051101',
                'name' => 'Vilcas Huaman',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '051102',
                'name' => 'Accomarca',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '051103',
                'name' => 'Carhuanca',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '051104',
                'name' => 'Concepción',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '051105',
                'name' => 'Huambalpa',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '051106',
                'name' => 'Independencia',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '051107',
                'name' => 'Saurama',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '051108',
                'name' => 'Vischongo',
                'region_id' => '050000',
                'province_id' => '051100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '060101',
                'name' => 'Cajamarca',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '060102',
                'name' => 'Asunción',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '060103',
                'name' => 'Chetilla',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '060104',
                'name' => 'Cospan',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '060105',
                'name' => 'Encañada',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '060106',
                'name' => 'Jesús',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '060107',
                'name' => 'Llacanora',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '060108',
                'name' => 'Los Baños del Inca',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '060109',
                'name' => 'Magdalena',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '060110',
                'name' => 'Matara',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '060111',
                'name' => 'Namora',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '060112',
                'name' => 'San Juan',
                'region_id' => '060000',
                'province_id' => '060100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '060201',
                'name' => 'Cajabamba',
                'region_id' => '060000',
                'province_id' => '060200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '060202',
                'name' => 'Cachachi',
                'region_id' => '060000',
                'province_id' => '060200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '060203',
                'name' => 'Condebamba',
                'region_id' => '060000',
                'province_id' => '060200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '060204',
                'name' => 'Sitacocha',
                'region_id' => '060000',
                'province_id' => '060200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '060301',
                'name' => 'Celendín',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '060302',
                'name' => 'Chumuch',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '060303',
                'name' => 'Cortegana',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '060304',
                'name' => 'Huasmin',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '060305',
                'name' => 'Jorge Chávez',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '060306',
                'name' => 'José Gálvez',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '060307',
                'name' => 'Miguel Iglesias',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '060308',
                'name' => 'Oxamarca',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '060309',
                'name' => 'Sorochuco',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '060310',
                'name' => 'Sucre',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '060311',
                'name' => 'Utco',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '060312',
                'name' => 'La Libertad de Pallan',
                'region_id' => '060000',
                'province_id' => '060300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '060401',
                'name' => 'Chota',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '060402',
                'name' => 'Anguia',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '060403',
                'name' => 'Chadin',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '060404',
                'name' => 'Chiguirip',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '060405',
                'name' => 'Chimban',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '060406',
                'name' => 'Choropampa',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '060407',
                'name' => 'Cochabamba',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '060408',
                'name' => 'Conchan',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '060409',
                'name' => 'Huambos',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '060410',
                'name' => 'Lajas',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '060411',
                'name' => 'Llama',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '060412',
                'name' => 'Miracosta',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '060413',
                'name' => 'Paccha',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '060414',
                'name' => 'Pion',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '060415',
                'name' => 'Querocoto',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '060416',
                'name' => 'San Juan de Licupis',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '060417',
                'name' => 'Tacabamba',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '060418',
                'name' => 'Tocmoche',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '060419',
                'name' => 'Chalamarca',
                'region_id' => '060000',
                'province_id' => '060400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '060501',
                'name' => 'Contumaza',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '060502',
                'name' => 'Chilete',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '060503',
                'name' => 'Cupisnique',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '060504',
                'name' => 'Guzmango',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '060505',
                'name' => 'San Benito',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '060506',
                'name' => 'Santa Cruz de Toledo',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '060507',
                'name' => 'Tantarica',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '060508',
                'name' => 'Yonan',
                'region_id' => '060000',
                'province_id' => '060500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '060601',
                'name' => 'Cutervo',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '060602',
                'name' => 'Callayuc',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '060603',
                'name' => 'Choros',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '060604',
                'name' => 'Cujillo',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '060605',
                'name' => 'La Ramada',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '060606',
                'name' => 'Pimpingos',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '060607',
                'name' => 'Querocotillo',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '060608',
                'name' => 'San Andrés de Cutervo',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '060609',
                'name' => 'San Juan de Cutervo',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '060610',
                'name' => 'San Luis de Lucma',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '060611',
                'name' => 'Santa Cruz',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '060612',
                'name' => 'Santo Domingo de la Capilla',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '060613',
                'name' => 'Santo Tomas',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '060614',
                'name' => 'Socota',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '060615',
                'name' => 'Toribio Casanova',
                'region_id' => '060000',
                'province_id' => '060600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '060701',
                'name' => 'Bambamarca',
                'region_id' => '060000',
                'province_id' => '060700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '060702',
                'name' => 'Chugur',
                'region_id' => '060000',
                'province_id' => '060700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '060703',
                'name' => 'Hualgayoc',
                'region_id' => '060000',
                'province_id' => '060700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '060801',
                'name' => 'Jaén',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '060802',
                'name' => 'Bellavista',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '060803',
                'name' => 'Chontali',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '060804',
                'name' => 'Colasay',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '060805',
                'name' => 'Huabal',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '060806',
                'name' => 'Las Pirias',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '060807',
                'name' => 'Pomahuaca',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '060808',
                'name' => 'Pucara',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '060809',
                'name' => 'Sallique',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '060810',
                'name' => 'San Felipe',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '060811',
                'name' => 'San José del Alto',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '060812',
                'name' => 'Santa Rosa',
                'region_id' => '060000',
                'province_id' => '060800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '060901',
                'name' => 'San Ignacio',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '060902',
                'name' => 'Chirinos',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '060903',
                'name' => 'Huarango',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '060904',
                'name' => 'La Coipa',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '060905',
                'name' => 'Namballe',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '060906',
                'name' => 'San José de Lourdes',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '060907',
                'name' => 'Tabaconas',
                'region_id' => '060000',
                'province_id' => '060900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '061001',
                'name' => 'Pedro Gálvez',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '061002',
                'name' => 'Chancay',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '061003',
                'name' => 'Eduardo Villanueva',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '061004',
                'name' => 'Gregorio Pita',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '061005',
                'name' => 'Ichocan',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '061006',
                'name' => 'José Manuel Quiroz',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '061007',
                'name' => 'José Sabogal',
                'region_id' => '060000',
                'province_id' => '061000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '061101',
                'name' => 'San Miguel',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '061102',
                'name' => 'Bolívar',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '061103',
                'name' => 'Calquis',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '061104',
                'name' => 'Catilluc',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '061105',
                'name' => 'El Prado',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '061106',
                'name' => 'La Florida',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '061107',
                'name' => 'Llapa',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '061108',
                'name' => 'Nanchoc',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '061109',
                'name' => 'Niepos',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '061110',
                'name' => 'San Gregorio',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '061111',
                'name' => 'San Silvestre de Cochan',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '061112',
                'name' => 'Tongod',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '061113',
                'name' => 'Unión Agua Blanca',
                'region_id' => '060000',
                'province_id' => '061100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '061201',
                'name' => 'San Pablo',
                'region_id' => '060000',
                'province_id' => '061200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '061202',
                'name' => 'San Bernardino',
                'region_id' => '060000',
                'province_id' => '061200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '061203',
                'name' => 'San Luis',
                'region_id' => '060000',
                'province_id' => '061200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '061204',
                'name' => 'Tumbaden',
                'region_id' => '060000',
                'province_id' => '061200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '061301',
                'name' => 'Santa Cruz',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '061302',
                'name' => 'Andabamba',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '061303',
                'name' => 'Catache',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '061304',
                'name' => 'Chancaybaños',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '061305',
                'name' => 'La Esperanza',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '061306',
                'name' => 'Ninabamba',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '061307',
                'name' => 'Pulan',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '061308',
                'name' => 'Saucepampa',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '061309',
                'name' => 'Sexi',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '061310',
                'name' => 'Uticyacu',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '061311',
                'name' => 'Yauyucan',
                'region_id' => '060000',
                'province_id' => '061300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '070101',
                'name' => 'Callao',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '070102',
                'name' => 'Bellavista',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '070103',
                'name' => 'Carmen de la Legua Reynoso',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '070104',
                'name' => 'La Perla',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '070105',
                'name' => 'La Punta',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '070106',
                'name' => 'Ventanilla',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '070107',
                'name' => 'Mi Perú',
                'region_id' => '070000',
                'province_id' => '070100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => '080101',
                'name' => 'Cusco',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => '080102',
                'name' => 'Ccorca',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => '080103',
                'name' => 'Poroy',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => '080104',
                'name' => 'San Jerónimo',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => '080105',
                'name' => 'San Sebastian',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => '080106',
                'name' => 'Santiago',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => '080107',
                'name' => 'Saylla',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => '080108',
                'name' => 'Wanchaq',
                'region_id' => '080000',
                'province_id' => '080100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => '080201',
                'name' => 'Acomayo',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => '080202',
                'name' => 'Acopia',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => '080203',
                'name' => 'Acos',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => '080204',
                'name' => 'Mosoc Llacta',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => '080205',
                'name' => 'Pomacanchi',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => '080206',
                'name' => 'Rondocan',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => '080207',
                'name' => 'Sangarara',
                'region_id' => '080000',
                'province_id' => '080200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => '080301',
                'name' => 'Anta',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => '080302',
                'name' => 'Ancahuasi',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => '080303',
                'name' => 'Cachimayo',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => '080304',
                'name' => 'Chinchaypujio',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => '080305',
                'name' => 'Huarocondo',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => '080306',
                'name' => 'Limatambo',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => '080307',
                'name' => 'Mollepata',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => '080308',
                'name' => 'Pucyura',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => '080309',
                'name' => 'Zurite',
                'region_id' => '080000',
                'province_id' => '080300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => '080401',
                'name' => 'Calca',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => '080402',
                'name' => 'Coya',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => '080403',
                'name' => 'Lamay',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => '080404',
                'name' => 'Lares',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => '080405',
                'name' => 'Pisac',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => '080406',
                'name' => 'San Salvador',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => '080407',
                'name' => 'Taray',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => '080408',
                'name' => 'Yanatile',
                'region_id' => '080000',
                'province_id' => '080400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => '080501',
                'name' => 'Yanaoca',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => '080502',
                'name' => 'Checca',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => '080503',
                'name' => 'Kunturkanki',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => '080504',
                'name' => 'Langui',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => '080505',
                'name' => 'Layo',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => '080506',
                'name' => 'Pampamarca',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => '080507',
                'name' => 'Quehue',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => '080508',
                'name' => 'Tupac Amaru',
                'region_id' => '080000',
                'province_id' => '080500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => '080601',
                'name' => 'Sicuani',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => '080602',
                'name' => 'Checacupe',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => '080603',
                'name' => 'Combapata',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => '080604',
                'name' => 'Marangani',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => '080605',
                'name' => 'Pitumarca',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => '080606',
                'name' => 'San Pablo',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => '080607',
                'name' => 'San Pedro',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => '080608',
                'name' => 'Tinta',
                'region_id' => '080000',
                'province_id' => '080600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => '080701',
                'name' => 'Santo Tomas',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => '080702',
                'name' => 'Capacmarca',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => '080703',
                'name' => 'Chamaca',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => '080704',
                'name' => 'Colquemarca',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => '080705',
                'name' => 'Livitaca',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => '080706',
                'name' => 'Llusco',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => '080707',
                'name' => 'Quiñota',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => '080708',
                'name' => 'Velille',
                'region_id' => '080000',
                'province_id' => '080700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => '080801',
                'name' => 'Espinar',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => '080802',
                'name' => 'Condoroma',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => '080803',
                'name' => 'Coporaque',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => '080804',
                'name' => 'Ocoruro',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => '080805',
                'name' => 'Pallpata',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => '080806',
                'name' => 'Pichigua',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => '080807',
                'name' => 'Suyckutambo',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => '080808',
                'name' => 'Alto Pichigua',
                'region_id' => '080000',
                'province_id' => '080800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => '080901',
                'name' => 'Santa Ana',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => '080902',
                'name' => 'Echarate',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => '080903',
                'name' => 'Huayopata',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => '080904',
                'name' => 'Maranura',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => '080905',
                'name' => 'Ocobamba',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => '080906',
                'name' => 'Quellouno',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => '080907',
                'name' => 'Kimbiri',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => '080908',
                'name' => 'Santa Teresa',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => '080909',
                'name' => 'Vilcabamba',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => '080910',
                'name' => 'Pichari',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => '080911',
                'name' => 'Inkawasi',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => '080912',
                'name' => 'Villa Virgen',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => '080913',
                'name' => 'Villa Kintiarina',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => '080914',
                'name' => 'Megantoni',
                'region_id' => '080000',
                'province_id' => '080900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => '081001',
                'name' => 'Paruro',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => '081002',
                'name' => 'Accha',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => '081003',
                'name' => 'Ccapi',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => '081004',
                'name' => 'Colcha',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => '081005',
                'name' => 'Huanoquite',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => '081006',
                'name' => 'Omacha',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => '081007',
                'name' => 'Paccaritambo',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => '081008',
                'name' => 'Pillpinto',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => '081009',
                'name' => 'Yaurisque',
                'region_id' => '080000',
                'province_id' => '081000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => '081101',
                'name' => 'Paucartambo',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => '081102',
                'name' => 'Caicay',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => '081103',
                'name' => 'Challabamba',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => '081104',
                'name' => 'Colquepata',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => '081105',
                'name' => 'Huancarani',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => '081106',
                'name' => 'Kosñipata',
                'region_id' => '080000',
                'province_id' => '081100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => '081201',
                'name' => 'Urcos',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => '081202',
                'name' => 'Andahuaylillas',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => '081203',
                'name' => 'Camanti',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => '081204',
                'name' => 'Ccarhuayo',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => '081205',
                'name' => 'Ccatca',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => '081206',
                'name' => 'Cusipata',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => '081207',
                'name' => 'Huaro',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => '081208',
                'name' => 'Lucre',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => '081209',
                'name' => 'Marcapata',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => '081210',
                'name' => 'Ocongate',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => '081211',
                'name' => 'Oropesa',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => '081212',
                'name' => 'Quiquijana',
                'region_id' => '080000',
                'province_id' => '081200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => '081301',
                'name' => 'Urubamba',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => '081302',
                'name' => 'Chinchero',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => '081303',
                'name' => 'Huayllabamba',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => '081304',
                'name' => 'Machupicchu',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => '081305',
                'name' => 'Maras',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => '081306',
                'name' => 'Ollantaytambo',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => '081307',
                'name' => 'Yucay',
                'region_id' => '080000',
                'province_id' => '081300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => '090101',
                'name' => 'Huancavelica',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => '090102',
                'name' => 'Acobambilla',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => '090103',
                'name' => 'Acoria',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => '090104',
                'name' => 'Conayca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => '090105',
                'name' => 'Cuenca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => '090106',
                'name' => 'Huachocolpa',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => '090107',
                'name' => 'Huayllahuara',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => '090108',
                'name' => 'Izcuchaca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => '090109',
                'name' => 'Laria',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => '090110',
                'name' => 'Manta',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => '090111',
                'name' => 'Mariscal Cáceres',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => '090112',
                'name' => 'Moya',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => '090113',
                'name' => 'Nuevo Occoro',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => '090114',
                'name' => 'Palca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => '090115',
                'name' => 'Pilchaca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => '090116',
                'name' => 'Vilca',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => '090117',
                'name' => 'Yauli',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => '090118',
                'name' => 'Ascensión',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => '090119',
                'name' => 'Huando',
                'region_id' => '090000',
                'province_id' => '090100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => '090201',
                'name' => 'Acobamba',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => '090202',
                'name' => 'Andabamba',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => '090203',
                'name' => 'Anta',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => '090204',
                'name' => 'Caja',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => '090205',
                'name' => 'Marcas',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => '090206',
                'name' => 'Paucara',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => '090207',
                'name' => 'Pomacocha',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => '090208',
                'name' => 'Rosario',
                'region_id' => '090000',
                'province_id' => '090200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => '090301',
                'name' => 'Lircay',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => '090302',
                'name' => 'Anchonga',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => '090303',
                'name' => 'Callanmarca',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => '090304',
                'name' => 'Ccochaccasa',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => '090305',
                'name' => 'Chincho',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => '090306',
                'name' => 'Congalla',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => '090307',
                'name' => 'Huanca-Huanca',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => '090308',
                'name' => 'Huayllay Grande',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => '090309',
                'name' => 'Julcamarca',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => '090310',
                'name' => 'San Antonio de Antaparco',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => '090311',
                'name' => 'Santo Tomas de Pata',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => '090312',
                'name' => 'Secclla',
                'region_id' => '090000',
                'province_id' => '090300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => '090401',
                'name' => 'Castrovirreyna',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => '090402',
                'name' => 'Arma',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => '090403',
                'name' => 'Aurahua',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => '090404',
                'name' => 'Capillas',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => '090405',
                'name' => 'Chupamarca',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => '090406',
                'name' => 'Cocas',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => '090407',
                'name' => 'Huachos',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => '090408',
                'name' => 'Huamatambo',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => '090409',
                'name' => 'Mollepampa',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => '090410',
                'name' => 'San Juan',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => '090411',
                'name' => 'Santa Ana',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => '090412',
                'name' => 'Tantara',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => '090413',
                'name' => 'Ticrapo',
                'region_id' => '090000',
                'province_id' => '090400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => '090501',
                'name' => 'Churcampa',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => '090502',
                'name' => 'Anco',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => '090503',
                'name' => 'Chinchihuasi',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => '090504',
                'name' => 'El Carmen',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => '090505',
                'name' => 'La Merced',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => '090506',
                'name' => 'Locroja',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => '090507',
                'name' => 'Paucarbamba',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => '090508',
                'name' => 'San Miguel de Mayocc',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => '090509',
                'name' => 'San Pedro de Coris',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => '090510',
                'name' => 'Pachamarca',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => '090511',
                'name' => 'Cosme',
                'region_id' => '090000',
                'province_id' => '090500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => '090601',
                'name' => 'Huaytara',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => '090602',
                'name' => 'Ayavi',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => '090603',
                'name' => 'Córdova',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => '090604',
                'name' => 'Huayacundo Arma',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => '090605',
                'name' => 'Laramarca',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => '090606',
                'name' => 'Ocoyo',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => '090607',
                'name' => 'Pilpichaca',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => '090608',
                'name' => 'Querco',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => '090609',
                'name' => 'Quito-Arma',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => '090610',
                'name' => 'San Antonio de Cusicancha',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => '090611',
                'name' => 'San Francisco de Sangayaico',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => '090612',
                'name' => 'San Isidro',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => '090613',
                'name' => 'Santiago de Chocorvos',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => '090614',
                'name' => 'Santiago de Quirahuara',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => '090615',
                'name' => 'Santo Domingo de Capillas',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => '090616',
                'name' => 'Tambo',
                'region_id' => '090000',
                'province_id' => '090600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => '090701',
                'name' => 'Pampas',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => '090702',
                'name' => 'Acostambo',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => '090703',
                'name' => 'Acraquia',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => '090704',
                'name' => 'Ahuaycha',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => '090705',
                'name' => 'Colcabamba',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => '090706',
                'name' => 'Daniel Hernández',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => '090707',
                'name' => 'Huachocolpa',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => '090709',
                'name' => 'Huaribamba',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => '090710',
                'name' => 'Ñahuimpuquio',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => '090711',
                'name' => 'Pazos',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => '090713',
                'name' => 'Quishuar',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => '090714',
                'name' => 'Salcabamba',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => '090715',
                'name' => 'Salcahuasi',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => '090716',
                'name' => 'San Marcos de Rocchac',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => '090717',
                'name' => 'Surcubamba',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => '090718',
                'name' => 'Tintay Puncu',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => '090719',
                'name' => 'Quichuas',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => '090720',
                'name' => 'Andaymarca',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => '090721',
                'name' => 'Roble',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => '090722',
                'name' => 'Pichos',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => '090723',
                'name' => 'Santiago de Tucuma',
                'region_id' => '090000',
                'province_id' => '090700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => '100101',
                'name' => 'Huanuco',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => '100102',
                'name' => 'Amarilis',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => '100103',
                'name' => 'Chinchao',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => '100104',
                'name' => 'Churubamba',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => '100105',
                'name' => 'Margos',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => '100106',
            'name' => 'Quisqui (Kichki)',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => '100107',
                'name' => 'San Francisco de Cayran',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => '100108',
                'name' => 'San Pedro de Chaulan',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => '100109',
                'name' => 'Santa María del Valle',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => '100110',
                'name' => 'Yarumayo',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => '100111',
                'name' => 'Pillco Marca',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => '100112',
                'name' => 'Yacus',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => '100113',
                'name' => 'San Pablo de Pillao',
                'region_id' => '100000',
                'province_id' => '100100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => '100201',
                'name' => 'Ambo',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => '100202',
                'name' => 'Cayna',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => '100203',
                'name' => 'Colpas',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => '100204',
                'name' => 'Conchamarca',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => '100205',
                'name' => 'Huacar',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => '100206',
                'name' => 'San Francisco',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => '100207',
                'name' => 'San Rafael',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => '100208',
                'name' => 'Tomay Kichwa',
                'region_id' => '100000',
                'province_id' => '100200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => '100301',
                'name' => 'La Unión',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => '100307',
                'name' => 'Chuquis',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => '100311',
                'name' => 'Marías',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => '100313',
                'name' => 'Pachas',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => '100316',
                'name' => 'Quivilla',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => '100317',
                'name' => 'Ripan',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => '100321',
                'name' => 'Shunqui',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => '100322',
                'name' => 'Sillapata',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => '100323',
                'name' => 'Yanas',
                'region_id' => '100000',
                'province_id' => '100300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => '100401',
                'name' => 'Huacaybamba',
                'region_id' => '100000',
                'province_id' => '100400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => '100402',
                'name' => 'Canchabamba',
                'region_id' => '100000',
                'province_id' => '100400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => '100403',
                'name' => 'Cochabamba',
                'region_id' => '100000',
                'province_id' => '100400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => '100404',
                'name' => 'Pinra',
                'region_id' => '100000',
                'province_id' => '100400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => '100501',
                'name' => 'Llata',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => '100502',
                'name' => 'Arancay',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => '100503',
                'name' => 'Chavín de Pariarca',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => '100504',
                'name' => 'Jacas Grande',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => '100505',
                'name' => 'Jircan',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => '100506',
                'name' => 'Miraflores',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => '100507',
                'name' => 'Monzón',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => '100508',
                'name' => 'Punchao',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => '100509',
                'name' => 'Puños',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => '100510',
                'name' => 'Singa',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => '100511',
                'name' => 'Tantamayo',
                'region_id' => '100000',
                'province_id' => '100500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => '100601',
                'name' => 'Rupa-Rupa',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => '100602',
                'name' => 'Daniel Alomía Robles',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => '100603',
                'name' => 'Hermílio Valdizan',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => '100604',
                'name' => 'José Crespo y Castillo',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => '100605',
                'name' => 'Luyando',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => '100606',
                'name' => 'Mariano Damaso Beraun',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => '100607',
                'name' => 'Pucayacu',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => '100608',
                'name' => 'Castillo Grande',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => '100609',
                'name' => 'Pueblo Nuevo',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => '100610',
                'name' => 'Santo Domingo de Anda',
                'region_id' => '100000',
                'province_id' => '100600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => '100701',
                'name' => 'Huacrachuco',
                'region_id' => '100000',
                'province_id' => '100700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => '100702',
                'name' => 'Cholon',
                'region_id' => '100000',
                'province_id' => '100700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => '100703',
                'name' => 'San Buenaventura',
                'region_id' => '100000',
                'province_id' => '100700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => '100704',
                'name' => 'La Morada',
                'region_id' => '100000',
                'province_id' => '100700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => '100705',
                'name' => 'Santa Rosa de Alto Yanajanca',
                'region_id' => '100000',
                'province_id' => '100700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => '100801',
                'name' => 'Panao',
                'region_id' => '100000',
                'province_id' => '100800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => '100802',
                'name' => 'Chaglla',
                'region_id' => '100000',
                'province_id' => '100800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => '100803',
                'name' => 'Molino',
                'region_id' => '100000',
                'province_id' => '100800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => '100804',
                'name' => 'Umari',
                'region_id' => '100000',
                'province_id' => '100800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => '100901',
                'name' => 'Puerto Inca',
                'region_id' => '100000',
                'province_id' => '100900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => '100902',
                'name' => 'Codo del Pozuzo',
                'region_id' => '100000',
                'province_id' => '100900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => '100903',
                'name' => 'Honoria',
                'region_id' => '100000',
                'province_id' => '100900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => '100904',
                'name' => 'Tournavista',
                'region_id' => '100000',
                'province_id' => '100900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => '100905',
                'name' => 'Yuyapichis',
                'region_id' => '100000',
                'province_id' => '100900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => '101001',
                'name' => 'Jesús',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => '101002',
                'name' => 'Baños',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => '101003',
                'name' => 'Jivia',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => '101004',
                'name' => 'Queropalca',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => '101005',
                'name' => 'Rondos',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => '101006',
                'name' => 'San Francisco de Asís',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => '101007',
                'name' => 'San Miguel de Cauri',
                'region_id' => '100000',
                'province_id' => '101000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => '101101',
                'name' => 'Chavinillo',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => '101102',
                'name' => 'Cahuac',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => '101103',
                'name' => 'Chacabamba',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => '101104',
                'name' => 'Aparicio Pomares',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => '101105',
                'name' => 'Jacas Chico',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => '101106',
                'name' => 'Obas',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => '101107',
                'name' => 'Pampamarca',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => '101108',
                'name' => 'Choras',
                'region_id' => '100000',
                'province_id' => '101100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => '110101',
                'name' => 'Ica',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => '110102',
                'name' => 'La Tinguiña',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => '110103',
                'name' => 'Los Aquijes',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => '110104',
                'name' => 'Ocucaje',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => '110105',
                'name' => 'Pachacutec',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => '110106',
                'name' => 'Parcona',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => '110107',
                'name' => 'Pueblo Nuevo',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => '110108',
                'name' => 'Salas',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('districts')->insert(array (
            0 => 
            array (
                'id' => '110109',
                'name' => 'San José de Los Molinos',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '110110',
                'name' => 'San Juan Bautista',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '110111',
                'name' => 'Santiago',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '110112',
                'name' => 'Subtanjalla',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '110113',
                'name' => 'Tate',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '110114',
                'name' => 'Yauca del Rosario',
                'region_id' => '110000',
                'province_id' => '110100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '110201',
                'name' => 'Chincha Alta',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '110202',
                'name' => 'Alto Laran',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '110203',
                'name' => 'Chavin',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '110204',
                'name' => 'Chincha Baja',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '110205',
                'name' => 'El Carmen',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '110206',
                'name' => 'Grocio Prado',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '110207',
                'name' => 'Pueblo Nuevo',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '110208',
                'name' => 'San Juan de Yanac',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '110209',
                'name' => 'San Pedro de Huacarpana',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '110210',
                'name' => 'Sunampe',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '110211',
                'name' => 'Tambo de Mora',
                'region_id' => '110000',
                'province_id' => '110200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '110301',
                'name' => 'Nasca',
                'region_id' => '110000',
                'province_id' => '110300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '110302',
                'name' => 'Changuillo',
                'region_id' => '110000',
                'province_id' => '110300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '110303',
                'name' => 'El Ingenio',
                'region_id' => '110000',
                'province_id' => '110300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '110304',
                'name' => 'Marcona',
                'region_id' => '110000',
                'province_id' => '110300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '110305',
                'name' => 'Vista Alegre',
                'region_id' => '110000',
                'province_id' => '110300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '110401',
                'name' => 'Palpa',
                'region_id' => '110000',
                'province_id' => '110400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '110402',
                'name' => 'Llipata',
                'region_id' => '110000',
                'province_id' => '110400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '110403',
                'name' => 'Río Grande',
                'region_id' => '110000',
                'province_id' => '110400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '110404',
                'name' => 'Santa Cruz',
                'region_id' => '110000',
                'province_id' => '110400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '110405',
                'name' => 'Tibillo',
                'region_id' => '110000',
                'province_id' => '110400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '110501',
                'name' => 'Pisco',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '110502',
                'name' => 'Huancano',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '110503',
                'name' => 'Humay',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '110504',
                'name' => 'Independencia',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '110505',
                'name' => 'Paracas',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '110506',
                'name' => 'San Andrés',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '110507',
                'name' => 'San Clemente',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '110508',
                'name' => 'Tupac Amaru Inca',
                'region_id' => '110000',
                'province_id' => '110500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '120101',
                'name' => 'Huancayo',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '120104',
                'name' => 'Carhuacallanga',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '120105',
                'name' => 'Chacapampa',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '120106',
                'name' => 'Chicche',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '120107',
                'name' => 'Chilca',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '120108',
                'name' => 'Chongos Alto',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '120111',
                'name' => 'Chupuro',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '120112',
                'name' => 'Colca',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '120113',
                'name' => 'Cullhuas',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '120114',
                'name' => 'El Tambo',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '120116',
                'name' => 'Huacrapuquio',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '120117',
                'name' => 'Hualhuas',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '120119',
                'name' => 'Huancan',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '120120',
                'name' => 'Huasicancha',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '120121',
                'name' => 'Huayucachi',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '120122',
                'name' => 'Ingenio',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '120124',
                'name' => 'Pariahuanca',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '120125',
                'name' => 'Pilcomayo',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '120126',
                'name' => 'Pucara',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '120127',
                'name' => 'Quichuay',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '120128',
                'name' => 'Quilcas',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '120129',
                'name' => 'San Agustín',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '120130',
                'name' => 'San Jerónimo de Tunan',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '120132',
                'name' => 'Saño',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '120133',
                'name' => 'Sapallanga',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '120134',
                'name' => 'Sicaya',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '120135',
                'name' => 'Santo Domingo de Acobamba',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '120136',
                'name' => 'Viques',
                'region_id' => '120000',
                'province_id' => '120100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '120201',
                'name' => 'Concepción',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '120202',
                'name' => 'Aco',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '120203',
                'name' => 'Andamarca',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '120204',
                'name' => 'Chambara',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '120205',
                'name' => 'Cochas',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '120206',
                'name' => 'Comas',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '120207',
                'name' => 'Heroínas Toledo',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '120208',
                'name' => 'Manzanares',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '120209',
                'name' => 'Mariscal Castilla',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '120210',
                'name' => 'Matahuasi',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '120211',
                'name' => 'Mito',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '120212',
                'name' => 'Nueve de Julio',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '120213',
                'name' => 'Orcotuna',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '120214',
                'name' => 'San José de Quero',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '120215',
                'name' => 'Santa Rosa de Ocopa',
                'region_id' => '120000',
                'province_id' => '120200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '120301',
                'name' => 'Chanchamayo',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '120302',
                'name' => 'Perene',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '120303',
                'name' => 'Pichanaqui',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '120304',
                'name' => 'San Luis de Shuaro',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '120305',
                'name' => 'San Ramón',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '120306',
                'name' => 'Vitoc',
                'region_id' => '120000',
                'province_id' => '120300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '120401',
                'name' => 'Jauja',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '120402',
                'name' => 'Acolla',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '120403',
                'name' => 'Apata',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '120404',
                'name' => 'Ataura',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '120405',
                'name' => 'Canchayllo',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '120406',
                'name' => 'Curicaca',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '120407',
                'name' => 'El Mantaro',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '120408',
                'name' => 'Huamali',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '120409',
                'name' => 'Huaripampa',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '120410',
                'name' => 'Huertas',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '120411',
                'name' => 'Janjaillo',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '120412',
                'name' => 'Julcán',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '120413',
                'name' => 'Leonor Ordóñez',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '120414',
                'name' => 'Llocllapampa',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '120415',
                'name' => 'Marco',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '120416',
                'name' => 'Masma',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '120417',
                'name' => 'Masma Chicche',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '120418',
                'name' => 'Molinos',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '120419',
                'name' => 'Monobamba',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '120420',
                'name' => 'Muqui',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '120421',
                'name' => 'Muquiyauyo',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '120422',
                'name' => 'Paca',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '120423',
                'name' => 'Paccha',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '120424',
                'name' => 'Pancan',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '120425',
                'name' => 'Parco',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '120426',
                'name' => 'Pomacancha',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '120427',
                'name' => 'Ricran',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '120428',
                'name' => 'San Lorenzo',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '120429',
                'name' => 'San Pedro de Chunan',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '120430',
                'name' => 'Sausa',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '120431',
                'name' => 'Sincos',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '120432',
                'name' => 'Tunan Marca',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '120433',
                'name' => 'Yauli',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '120434',
                'name' => 'Yauyos',
                'region_id' => '120000',
                'province_id' => '120400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '120501',
                'name' => 'Junin',
                'region_id' => '120000',
                'province_id' => '120500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '120502',
                'name' => 'Carhuamayo',
                'region_id' => '120000',
                'province_id' => '120500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '120503',
                'name' => 'Ondores',
                'region_id' => '120000',
                'province_id' => '120500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '120504',
                'name' => 'Ulcumayo',
                'region_id' => '120000',
                'province_id' => '120500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '120601',
                'name' => 'Satipo',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '120602',
                'name' => 'Coviriali',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '120603',
                'name' => 'Llaylla',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '120604',
                'name' => 'Mazamari',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '120605',
                'name' => 'Pampa Hermosa',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '120606',
                'name' => 'Pangoa',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '120607',
                'name' => 'Río Negro',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '120608',
                'name' => 'Río Tambo',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '120609',
                'name' => 'Vizcatan del Ene',
                'region_id' => '120000',
                'province_id' => '120600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '120701',
                'name' => 'Tarma',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '120702',
                'name' => 'Acobamba',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '120703',
                'name' => 'Huaricolca',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '120704',
                'name' => 'Huasahuasi',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '120705',
                'name' => 'La Unión',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '120706',
                'name' => 'Palca',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '120707',
                'name' => 'Palcamayo',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '120708',
                'name' => 'San Pedro de Cajas',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '120709',
                'name' => 'Tapo',
                'region_id' => '120000',
                'province_id' => '120700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '120801',
                'name' => 'La Oroya',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '120802',
                'name' => 'Chacapalpa',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '120803',
                'name' => 'Huay-Huay',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '120804',
                'name' => 'Marcapomacocha',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '120805',
                'name' => 'Morococha',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '120806',
                'name' => 'Paccha',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '120807',
                'name' => 'Santa Bárbara de Carhuacayan',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '120808',
                'name' => 'Santa Rosa de Sacco',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '120809',
                'name' => 'Suitucancha',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '120810',
                'name' => 'Yauli',
                'region_id' => '120000',
                'province_id' => '120800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '120901',
                'name' => 'Chupaca',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '120902',
                'name' => 'Ahuac',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '120903',
                'name' => 'Chongos Bajo',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '120904',
                'name' => 'Huachac',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '120905',
                'name' => 'Huamancaca Chico',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '120906',
                'name' => 'San Juan de Iscos',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '120907',
                'name' => 'San Juan de Jarpa',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '120908',
                'name' => 'Tres de Diciembre',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '120909',
                'name' => 'Yanacancha',
                'region_id' => '120000',
                'province_id' => '120900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '130101',
                'name' => 'Trujillo',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '130102',
                'name' => 'El Porvenir',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '130103',
                'name' => 'Florencia de Mora',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '130104',
                'name' => 'Huanchaco',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '130105',
                'name' => 'La Esperanza',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '130106',
                'name' => 'Laredo',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '130107',
                'name' => 'Moche',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '130108',
                'name' => 'Poroto',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '130109',
                'name' => 'Salaverry',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '130110',
                'name' => 'Simbal',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '130111',
                'name' => 'Victor Larco Herrera',
                'region_id' => '130000',
                'province_id' => '130100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '130201',
                'name' => 'Ascope',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '130202',
                'name' => 'Chicama',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '130203',
                'name' => 'Chocope',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '130204',
                'name' => 'Magdalena de Cao',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '130205',
                'name' => 'Paijan',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '130206',
                'name' => 'Rázuri',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '130207',
                'name' => 'Santiago de Cao',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '130208',
                'name' => 'Casa Grande',
                'region_id' => '130000',
                'province_id' => '130200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '130301',
                'name' => 'Bolívar',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '130302',
                'name' => 'Bambamarca',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '130303',
                'name' => 'Condormarca',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '130304',
                'name' => 'Longotea',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '130305',
                'name' => 'Uchumarca',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '130306',
                'name' => 'Ucuncha',
                'region_id' => '130000',
                'province_id' => '130300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '130401',
                'name' => 'Chepen',
                'region_id' => '130000',
                'province_id' => '130400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '130402',
                'name' => 'Pacanga',
                'region_id' => '130000',
                'province_id' => '130400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '130403',
                'name' => 'Pueblo Nuevo',
                'region_id' => '130000',
                'province_id' => '130400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '130501',
                'name' => 'Julcan',
                'region_id' => '130000',
                'province_id' => '130500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '130502',
                'name' => 'Calamarca',
                'region_id' => '130000',
                'province_id' => '130500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '130503',
                'name' => 'Carabamba',
                'region_id' => '130000',
                'province_id' => '130500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '130504',
                'name' => 'Huaso',
                'region_id' => '130000',
                'province_id' => '130500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '130601',
                'name' => 'Otuzco',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '130602',
                'name' => 'Agallpampa',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '130604',
                'name' => 'Charat',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '130605',
                'name' => 'Huaranchal',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '130606',
                'name' => 'La Cuesta',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => '130608',
                'name' => 'Mache',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => '130610',
                'name' => 'Paranday',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => '130611',
                'name' => 'Salpo',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => '130613',
                'name' => 'Sinsicap',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => '130614',
                'name' => 'Usquil',
                'region_id' => '130000',
                'province_id' => '130600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => '130701',
                'name' => 'San Pedro de Lloc',
                'region_id' => '130000',
                'province_id' => '130700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => '130702',
                'name' => 'Guadalupe',
                'region_id' => '130000',
                'province_id' => '130700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => '130703',
                'name' => 'Jequetepeque',
                'region_id' => '130000',
                'province_id' => '130700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => '130704',
                'name' => 'Pacasmayo',
                'region_id' => '130000',
                'province_id' => '130700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => '130705',
                'name' => 'San José',
                'region_id' => '130000',
                'province_id' => '130700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => '130801',
                'name' => 'Tayabamba',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => '130802',
                'name' => 'Buldibuyo',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => '130803',
                'name' => 'Chillia',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => '130804',
                'name' => 'Huancaspata',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => '130805',
                'name' => 'Huaylillas',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => '130806',
                'name' => 'Huayo',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => '130807',
                'name' => 'Ongon',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => '130808',
                'name' => 'Parcoy',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => '130809',
                'name' => 'Pataz',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => '130810',
                'name' => 'Pias',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => '130811',
                'name' => 'Santiago de Challas',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => '130812',
                'name' => 'Taurija',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => '130813',
                'name' => 'Urpay',
                'region_id' => '130000',
                'province_id' => '130800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => '130901',
                'name' => 'Huamachuco',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => '130902',
                'name' => 'Chugay',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => '130903',
                'name' => 'Cochorco',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => '130904',
                'name' => 'Curgos',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => '130905',
                'name' => 'Marcabal',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => '130906',
                'name' => 'Sanagoran',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => '130907',
                'name' => 'Sarin',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => '130908',
                'name' => 'Sartimbamba',
                'region_id' => '130000',
                'province_id' => '130900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => '131001',
                'name' => 'Santiago de Chuco',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => '131002',
                'name' => 'Angasmarca',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => '131003',
                'name' => 'Cachicadan',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => '131004',
                'name' => 'Mollebamba',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => '131005',
                'name' => 'Mollepata',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => '131006',
                'name' => 'Quiruvilca',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => '131007',
                'name' => 'Santa Cruz de Chuca',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => '131008',
                'name' => 'Sitabamba',
                'region_id' => '130000',
                'province_id' => '131000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => '131101',
                'name' => 'Cascas',
                'region_id' => '130000',
                'province_id' => '131100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => '131102',
                'name' => 'Lucma',
                'region_id' => '130000',
                'province_id' => '131100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => '131103',
                'name' => 'Marmot',
                'region_id' => '130000',
                'province_id' => '131100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => '131104',
                'name' => 'Sayapullo',
                'region_id' => '130000',
                'province_id' => '131100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => '131201',
                'name' => 'Viru',
                'region_id' => '130000',
                'province_id' => '131200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => '131202',
                'name' => 'Chao',
                'region_id' => '130000',
                'province_id' => '131200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => '131203',
                'name' => 'Guadalupito',
                'region_id' => '130000',
                'province_id' => '131200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => '140101',
                'name' => 'Chiclayo',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => '140102',
                'name' => 'Chongoyape',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => '140103',
                'name' => 'Eten',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => '140104',
                'name' => 'Eten Puerto',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => '140105',
                'name' => 'José Leonardo Ortiz',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => '140106',
                'name' => 'La Victoria',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => '140107',
                'name' => 'Lagunas',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => '140108',
                'name' => 'Monsefu',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => '140109',
                'name' => 'Nueva Arica',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => '140110',
                'name' => 'Oyotun',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => '140111',
                'name' => 'Picsi',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => '140112',
                'name' => 'Pimentel',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => '140113',
                'name' => 'Reque',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => '140114',
                'name' => 'Santa Rosa',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => '140115',
                'name' => 'Saña',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => '140116',
                'name' => 'Cayalti',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => '140117',
                'name' => 'Patapo',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => '140118',
                'name' => 'Pomalca',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => '140119',
                'name' => 'Pucala',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => '140120',
                'name' => 'Tuman',
                'region_id' => '140000',
                'province_id' => '140100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => '140201',
                'name' => 'Ferreñafe',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => '140202',
                'name' => 'Cañaris',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => '140203',
                'name' => 'Incahuasi',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => '140204',
                'name' => 'Manuel Antonio Mesones Muro',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => '140205',
                'name' => 'Pitipo',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => '140206',
                'name' => 'Pueblo Nuevo',
                'region_id' => '140000',
                'province_id' => '140200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => '140301',
                'name' => 'Lambayeque',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => '140302',
                'name' => 'Chochope',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => '140303',
                'name' => 'Illimo',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => '140304',
                'name' => 'Jayanca',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => '140305',
                'name' => 'Mochumi',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => '140306',
                'name' => 'Morrope',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => '140307',
                'name' => 'Motupe',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => '140308',
                'name' => 'Olmos',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => '140309',
                'name' => 'Pacora',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => '140310',
                'name' => 'Salas',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => '140311',
                'name' => 'San José',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => '140312',
                'name' => 'Tucume',
                'region_id' => '140000',
                'province_id' => '140300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => '150101',
                'name' => 'Lima',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => '150102',
                'name' => 'Ancón',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => '150103',
                'name' => 'Ate',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => '150104',
                'name' => 'Barranco',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => '150105',
                'name' => 'Breña',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => '150106',
                'name' => 'Carabayllo',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => '150107',
                'name' => 'Chaclacayo',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => '150108',
                'name' => 'Chorrillos',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => '150109',
                'name' => 'Cieneguilla',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => '150110',
                'name' => 'Comas',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => '150111',
                'name' => 'El Agustino',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => '150112',
                'name' => 'Independencia',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => '150113',
                'name' => 'Jesús María',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => '150114',
                'name' => 'La Molina',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => '150115',
                'name' => 'La Victoria',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => '150116',
                'name' => 'Lince',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => '150117',
                'name' => 'Los Olivos',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => '150118',
                'name' => 'Lurigancho',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => '150119',
                'name' => 'Lurin',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => '150120',
                'name' => 'Magdalena del Mar',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => '150121',
                'name' => 'Pueblo Libre',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => '150122',
                'name' => 'Miraflores',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => '150123',
                'name' => 'Pachacamac',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => '150124',
                'name' => 'Pucusana',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => '150125',
                'name' => 'Puente Piedra',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => '150126',
                'name' => 'Punta Hermosa',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => '150127',
                'name' => 'Punta Negra',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => '150128',
                'name' => 'Rímac',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => '150129',
                'name' => 'San Bartolo',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => '150130',
                'name' => 'San Borja',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => '150131',
                'name' => 'San Isidro',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => '150132',
                'name' => 'San Juan de Lurigancho',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => '150133',
                'name' => 'San Juan de Miraflores',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => '150134',
                'name' => 'San Luis',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => '150135',
                'name' => 'San Martín de Porres',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => '150136',
                'name' => 'San Miguel',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => '150137',
                'name' => 'Santa Anita',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => '150138',
                'name' => 'Santa María del Mar',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => '150139',
                'name' => 'Santa Rosa',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => '150140',
                'name' => 'Santiago de Surco',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => '150141',
                'name' => 'Surquillo',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => '150142',
                'name' => 'Villa El Salvador',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => '150143',
                'name' => 'Villa María del Triunfo',
                'region_id' => '150000',
                'province_id' => '150100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => '150201',
                'name' => 'Barranca',
                'region_id' => '150000',
                'province_id' => '150200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => '150202',
                'name' => 'Paramonga',
                'region_id' => '150000',
                'province_id' => '150200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => '150203',
                'name' => 'Pativilca',
                'region_id' => '150000',
                'province_id' => '150200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => '150204',
                'name' => 'Supe',
                'region_id' => '150000',
                'province_id' => '150200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => '150205',
                'name' => 'Supe Puerto',
                'region_id' => '150000',
                'province_id' => '150200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => '150301',
                'name' => 'Cajatambo',
                'region_id' => '150000',
                'province_id' => '150300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => '150302',
                'name' => 'Copa',
                'region_id' => '150000',
                'province_id' => '150300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => '150303',
                'name' => 'Gorgor',
                'region_id' => '150000',
                'province_id' => '150300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => '150304',
                'name' => 'Huancapon',
                'region_id' => '150000',
                'province_id' => '150300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => '150305',
                'name' => 'Manas',
                'region_id' => '150000',
                'province_id' => '150300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => '150401',
                'name' => 'Canta',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => '150402',
                'name' => 'Arahuay',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => '150403',
                'name' => 'Huamantanga',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => '150404',
                'name' => 'Huaros',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => '150405',
                'name' => 'Lachaqui',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => '150406',
                'name' => 'San Buenaventura',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => '150407',
                'name' => 'Santa Rosa de Quives',
                'region_id' => '150000',
                'province_id' => '150400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => '150501',
                'name' => 'San Vicente de Cañete',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => '150502',
                'name' => 'Asia',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => '150503',
                'name' => 'Calango',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => '150504',
                'name' => 'Cerro Azul',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => '150505',
                'name' => 'Chilca',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => '150506',
                'name' => 'Coayllo',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => '150507',
                'name' => 'Imperial',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => '150508',
                'name' => 'Lunahuana',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => '150509',
                'name' => 'Mala',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => '150510',
                'name' => 'Nuevo Imperial',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => '150511',
                'name' => 'Pacaran',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => '150512',
                'name' => 'Quilmana',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => '150513',
                'name' => 'San Antonio',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => '150514',
                'name' => 'San Luis',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => '150515',
                'name' => 'Santa Cruz de Flores',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => '150516',
                'name' => 'Zúñiga',
                'region_id' => '150000',
                'province_id' => '150500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => '150601',
                'name' => 'Huaral',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => '150602',
                'name' => 'Atavillos Alto',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => '150603',
                'name' => 'Atavillos Bajo',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => '150604',
                'name' => 'Aucallama',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => '150605',
                'name' => 'Chancay',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => '150606',
                'name' => 'Ihuari',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => '150607',
                'name' => 'Lampian',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => '150608',
                'name' => 'Pacaraos',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => '150609',
                'name' => 'San Miguel de Acos',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => '150610',
                'name' => 'Santa Cruz de Andamarca',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => '150611',
                'name' => 'Sumbilca',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => '150612',
                'name' => 'Veintisiete de Noviembre',
                'region_id' => '150000',
                'province_id' => '150600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => '150701',
                'name' => 'Matucana',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => '150702',
                'name' => 'Antioquia',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => '150703',
                'name' => 'Callahuanca',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => '150704',
                'name' => 'Carampoma',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => '150705',
                'name' => 'Chicla',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => '150706',
                'name' => 'Cuenca',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            374 => 
            array (
                'id' => '150707',
                'name' => 'Huachupampa',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            375 => 
            array (
                'id' => '150708',
                'name' => 'Huanza',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            376 => 
            array (
                'id' => '150709',
                'name' => 'Huarochiri',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            377 => 
            array (
                'id' => '150710',
                'name' => 'Lahuaytambo',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            378 => 
            array (
                'id' => '150711',
                'name' => 'Langa',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            379 => 
            array (
                'id' => '150712',
                'name' => 'Laraos',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            380 => 
            array (
                'id' => '150713',
                'name' => 'Mariatana',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            381 => 
            array (
                'id' => '150714',
                'name' => 'Ricardo Palma',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            382 => 
            array (
                'id' => '150715',
                'name' => 'San Andrés de Tupicocha',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            383 => 
            array (
                'id' => '150716',
                'name' => 'San Antonio',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            384 => 
            array (
                'id' => '150717',
                'name' => 'San Bartolomé',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            385 => 
            array (
                'id' => '150718',
                'name' => 'San Damian',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            386 => 
            array (
                'id' => '150719',
                'name' => 'San Juan de Iris',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            387 => 
            array (
                'id' => '150720',
                'name' => 'San Juan de Tantaranche',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            388 => 
            array (
                'id' => '150721',
                'name' => 'San Lorenzo de Quinti',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            389 => 
            array (
                'id' => '150722',
                'name' => 'San Mateo',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            390 => 
            array (
                'id' => '150723',
                'name' => 'San Mateo de Otao',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            391 => 
            array (
                'id' => '150724',
                'name' => 'San Pedro de Casta',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            392 => 
            array (
                'id' => '150725',
                'name' => 'San Pedro de Huancayre',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            393 => 
            array (
                'id' => '150726',
                'name' => 'Sangallaya',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            394 => 
            array (
                'id' => '150727',
                'name' => 'Santa Cruz de Cocachacra',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            395 => 
            array (
                'id' => '150728',
                'name' => 'Santa Eulalia',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            396 => 
            array (
                'id' => '150729',
                'name' => 'Santiago de Anchucaya',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            397 => 
            array (
                'id' => '150730',
                'name' => 'Santiago de Tuna',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            398 => 
            array (
                'id' => '150731',
                'name' => 'Santo Domingo de Los Olleros',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            399 => 
            array (
                'id' => '150732',
                'name' => 'Surco',
                'region_id' => '150000',
                'province_id' => '150700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            400 => 
            array (
                'id' => '150801',
                'name' => 'Huacho',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            401 => 
            array (
                'id' => '150802',
                'name' => 'Ambar',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            402 => 
            array (
                'id' => '150803',
                'name' => 'Caleta de Carquin',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            403 => 
            array (
                'id' => '150804',
                'name' => 'Checras',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            404 => 
            array (
                'id' => '150805',
                'name' => 'Hualmay',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            405 => 
            array (
                'id' => '150806',
                'name' => 'Huaura',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            406 => 
            array (
                'id' => '150807',
                'name' => 'Leoncio Prado',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            407 => 
            array (
                'id' => '150808',
                'name' => 'Paccho',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            408 => 
            array (
                'id' => '150809',
                'name' => 'Santa Leonor',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            409 => 
            array (
                'id' => '150810',
                'name' => 'Santa María',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            410 => 
            array (
                'id' => '150811',
                'name' => 'Sayan',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            411 => 
            array (
                'id' => '150812',
                'name' => 'Vegueta',
                'region_id' => '150000',
                'province_id' => '150800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            412 => 
            array (
                'id' => '150901',
                'name' => 'Oyon',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            413 => 
            array (
                'id' => '150902',
                'name' => 'Andajes',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            414 => 
            array (
                'id' => '150903',
                'name' => 'Caujul',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            415 => 
            array (
                'id' => '150904',
                'name' => 'Cochamarca',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            416 => 
            array (
                'id' => '150905',
                'name' => 'Navan',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            417 => 
            array (
                'id' => '150906',
                'name' => 'Pachangara',
                'region_id' => '150000',
                'province_id' => '150900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            418 => 
            array (
                'id' => '151001',
                'name' => 'Yauyos',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            419 => 
            array (
                'id' => '151002',
                'name' => 'Alis',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            420 => 
            array (
                'id' => '151003',
                'name' => 'Allauca',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            421 => 
            array (
                'id' => '151004',
                'name' => 'Ayaviri',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            422 => 
            array (
                'id' => '151005',
                'name' => 'Azángaro',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            423 => 
            array (
                'id' => '151006',
                'name' => 'Cacra',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            424 => 
            array (
                'id' => '151007',
                'name' => 'Carania',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            425 => 
            array (
                'id' => '151008',
                'name' => 'Catahuasi',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            426 => 
            array (
                'id' => '151009',
                'name' => 'Chocos',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            427 => 
            array (
                'id' => '151010',
                'name' => 'Cochas',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            428 => 
            array (
                'id' => '151011',
                'name' => 'Colonia',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            429 => 
            array (
                'id' => '151012',
                'name' => 'Hongos',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            430 => 
            array (
                'id' => '151013',
                'name' => 'Huampara',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            431 => 
            array (
                'id' => '151014',
                'name' => 'Huancaya',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            432 => 
            array (
                'id' => '151015',
                'name' => 'Huangascar',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            433 => 
            array (
                'id' => '151016',
                'name' => 'Huantan',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            434 => 
            array (
                'id' => '151017',
                'name' => 'Huañec',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            435 => 
            array (
                'id' => '151018',
                'name' => 'Laraos',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            436 => 
            array (
                'id' => '151019',
                'name' => 'Lincha',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            437 => 
            array (
                'id' => '151020',
                'name' => 'Madean',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            438 => 
            array (
                'id' => '151021',
                'name' => 'Miraflores',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            439 => 
            array (
                'id' => '151022',
                'name' => 'Omas',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            440 => 
            array (
                'id' => '151023',
                'name' => 'Putinza',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            441 => 
            array (
                'id' => '151024',
                'name' => 'Quinches',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            442 => 
            array (
                'id' => '151025',
                'name' => 'Quinocay',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            443 => 
            array (
                'id' => '151026',
                'name' => 'San Joaquín',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            444 => 
            array (
                'id' => '151027',
                'name' => 'San Pedro de Pilas',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            445 => 
            array (
                'id' => '151028',
                'name' => 'Tanta',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            446 => 
            array (
                'id' => '151029',
                'name' => 'Tauripampa',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            447 => 
            array (
                'id' => '151030',
                'name' => 'Tomas',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            448 => 
            array (
                'id' => '151031',
                'name' => 'Tupe',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            449 => 
            array (
                'id' => '151032',
                'name' => 'Viñac',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            450 => 
            array (
                'id' => '151033',
                'name' => 'Vitis',
                'region_id' => '150000',
                'province_id' => '151000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            451 => 
            array (
                'id' => '160101',
                'name' => 'Iquitos',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            452 => 
            array (
                'id' => '160102',
                'name' => 'Alto Nanay',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            453 => 
            array (
                'id' => '160103',
                'name' => 'Fernando Lores',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            454 => 
            array (
                'id' => '160104',
                'name' => 'Indiana',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            455 => 
            array (
                'id' => '160105',
                'name' => 'Las Amazonas',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            456 => 
            array (
                'id' => '160106',
                'name' => 'Mazan',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            457 => 
            array (
                'id' => '160107',
                'name' => 'Napo',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            458 => 
            array (
                'id' => '160108',
                'name' => 'Punchana',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            459 => 
            array (
                'id' => '160110',
                'name' => 'Torres Causana',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            460 => 
            array (
                'id' => '160112',
                'name' => 'Belén',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            461 => 
            array (
                'id' => '160113',
                'name' => 'San Juan Bautista',
                'region_id' => '160000',
                'province_id' => '160100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            462 => 
            array (
                'id' => '160201',
                'name' => 'Yurimaguas',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            463 => 
            array (
                'id' => '160202',
                'name' => 'Balsapuerto',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            464 => 
            array (
                'id' => '160205',
                'name' => 'Jeberos',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            465 => 
            array (
                'id' => '160206',
                'name' => 'Lagunas',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            466 => 
            array (
                'id' => '160210',
                'name' => 'Santa Cruz',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            467 => 
            array (
                'id' => '160211',
                'name' => 'Teniente Cesar López Rojas',
                'region_id' => '160000',
                'province_id' => '160200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            468 => 
            array (
                'id' => '160301',
                'name' => 'Nauta',
                'region_id' => '160000',
                'province_id' => '160300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            469 => 
            array (
                'id' => '160302',
                'name' => 'Parinari',
                'region_id' => '160000',
                'province_id' => '160300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            470 => 
            array (
                'id' => '160303',
                'name' => 'Tigre',
                'region_id' => '160000',
                'province_id' => '160300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            471 => 
            array (
                'id' => '160304',
                'name' => 'Trompeteros',
                'region_id' => '160000',
                'province_id' => '160300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            472 => 
            array (
                'id' => '160305',
                'name' => 'Urarinas',
                'region_id' => '160000',
                'province_id' => '160300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            473 => 
            array (
                'id' => '160401',
                'name' => 'Ramón Castilla',
                'region_id' => '160000',
                'province_id' => '160400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            474 => 
            array (
                'id' => '160402',
                'name' => 'Pebas',
                'region_id' => '160000',
                'province_id' => '160400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            475 => 
            array (
                'id' => '160403',
                'name' => 'Yavari',
                'region_id' => '160000',
                'province_id' => '160400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            476 => 
            array (
                'id' => '160404',
                'name' => 'San Pablo',
                'region_id' => '160000',
                'province_id' => '160400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            477 => 
            array (
                'id' => '160501',
                'name' => 'Requena',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            478 => 
            array (
                'id' => '160502',
                'name' => 'Alto Tapiche',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            479 => 
            array (
                'id' => '160503',
                'name' => 'Capelo',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            480 => 
            array (
                'id' => '160504',
                'name' => 'Emilio San Martín',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            481 => 
            array (
                'id' => '160505',
                'name' => 'Maquia',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            482 => 
            array (
                'id' => '160506',
                'name' => 'Puinahua',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            483 => 
            array (
                'id' => '160507',
                'name' => 'Saquena',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            484 => 
            array (
                'id' => '160508',
                'name' => 'Soplin',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            485 => 
            array (
                'id' => '160509',
                'name' => 'Tapiche',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            486 => 
            array (
                'id' => '160510',
                'name' => 'Jenaro Herrera',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            487 => 
            array (
                'id' => '160511',
                'name' => 'Yaquerana',
                'region_id' => '160000',
                'province_id' => '160500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            488 => 
            array (
                'id' => '160601',
                'name' => 'Contamana',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            489 => 
            array (
                'id' => '160602',
                'name' => 'Inahuaya',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            490 => 
            array (
                'id' => '160603',
                'name' => 'Padre Márquez',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            491 => 
            array (
                'id' => '160604',
                'name' => 'Pampa Hermosa',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            492 => 
            array (
                'id' => '160605',
                'name' => 'Sarayacu',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            493 => 
            array (
                'id' => '160606',
                'name' => 'Vargas Guerra',
                'region_id' => '160000',
                'province_id' => '160600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            494 => 
            array (
                'id' => '160701',
                'name' => 'Barranca',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            495 => 
            array (
                'id' => '160702',
                'name' => 'Cahuapanas',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            496 => 
            array (
                'id' => '160703',
                'name' => 'Manseriche',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            497 => 
            array (
                'id' => '160704',
                'name' => 'Morona',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            498 => 
            array (
                'id' => '160705',
                'name' => 'Pastaza',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            499 => 
            array (
                'id' => '160706',
                'name' => 'Andoas',
                'region_id' => '160000',
                'province_id' => '160700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        \DB::table('districts')->insert(array (
            0 => 
            array (
                'id' => '160801',
                'name' => 'Putumayo',
                'region_id' => '160000',
                'province_id' => '160800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => '160802',
                'name' => 'Rosa Panduro',
                'region_id' => '160000',
                'province_id' => '160800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => '160803',
                'name' => 'Teniente Manuel Clavero',
                'region_id' => '160000',
                'province_id' => '160800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => '160804',
                'name' => 'Yaguas',
                'region_id' => '160000',
                'province_id' => '160800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => '170101',
                'name' => 'Tambopata',
                'region_id' => '170000',
                'province_id' => '170100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => '170102',
                'name' => 'Inambari',
                'region_id' => '170000',
                'province_id' => '170100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => '170103',
                'name' => 'Las Piedras',
                'region_id' => '170000',
                'province_id' => '170100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => '170104',
                'name' => 'Laberinto',
                'region_id' => '170000',
                'province_id' => '170100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => '170201',
                'name' => 'Manu',
                'region_id' => '170000',
                'province_id' => '170200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => '170202',
                'name' => 'Fitzcarrald',
                'region_id' => '170000',
                'province_id' => '170200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => '170203',
                'name' => 'Madre de Dios',
                'region_id' => '170000',
                'province_id' => '170200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => '170204',
                'name' => 'Huepetuhe',
                'region_id' => '170000',
                'province_id' => '170200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => '170301',
                'name' => 'Iñapari',
                'region_id' => '170000',
                'province_id' => '170300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => '170302',
                'name' => 'Iberia',
                'region_id' => '170000',
                'province_id' => '170300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => '170303',
                'name' => 'Tahuamanu',
                'region_id' => '170000',
                'province_id' => '170300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => '180101',
                'name' => 'Moquegua',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => '180102',
                'name' => 'Carumas',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => '180103',
                'name' => 'Cuchumbaya',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => '180104',
                'name' => 'Samegua',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => '180105',
                'name' => 'San Cristóbal',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => '180106',
                'name' => 'Torata',
                'region_id' => '180000',
                'province_id' => '180100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => '180201',
                'name' => 'Omate',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => '180202',
                'name' => 'Chojata',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => '180203',
                'name' => 'Coalaque',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => '180204',
                'name' => 'Ichuña',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => '180205',
                'name' => 'La Capilla',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => '180206',
                'name' => 'Lloque',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => '180207',
                'name' => 'Matalaque',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => '180208',
                'name' => 'Puquina',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => '180209',
                'name' => 'Quinistaquillas',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => '180210',
                'name' => 'Ubinas',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => '180211',
                'name' => 'Yunga',
                'region_id' => '180000',
                'province_id' => '180200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => '180301',
                'name' => 'Ilo',
                'region_id' => '180000',
                'province_id' => '180300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => '180302',
                'name' => 'El Algarrobal',
                'region_id' => '180000',
                'province_id' => '180300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => '180303',
                'name' => 'Pacocha',
                'region_id' => '180000',
                'province_id' => '180300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => '190101',
                'name' => 'Chaupimarca',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => '190102',
                'name' => 'Huachon',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => '190103',
                'name' => 'Huariaca',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => '190104',
                'name' => 'Huayllay',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => '190105',
                'name' => 'Ninacaca',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => '190106',
                'name' => 'Pallanchacra',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => '190107',
                'name' => 'Paucartambo',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => '190108',
                'name' => 'San Francisco de Asís de Yarusyacan',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => '190109',
                'name' => 'Simon Bolívar',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => '190110',
                'name' => 'Ticlacayan',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => '190111',
                'name' => 'Tinyahuarco',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => '190112',
                'name' => 'Vicco',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => '190113',
                'name' => 'Yanacancha',
                'region_id' => '190000',
                'province_id' => '190100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => '190201',
                'name' => 'Yanahuanca',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => '190202',
                'name' => 'Chacayan',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => '190203',
                'name' => 'Goyllarisquizga',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => '190204',
                'name' => 'Paucar',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => '190205',
                'name' => 'San Pedro de Pillao',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => '190206',
                'name' => 'Santa Ana de Tusi',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => '190207',
                'name' => 'Tapuc',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => '190208',
                'name' => 'Vilcabamba',
                'region_id' => '190000',
                'province_id' => '190200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => '190301',
                'name' => 'Oxapampa',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => '190302',
                'name' => 'Chontabamba',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => '190303',
                'name' => 'Huancabamba',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => '190304',
                'name' => 'Palcazu',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => '190305',
                'name' => 'Pozuzo',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => '190306',
                'name' => 'Puerto Bermúdez',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => '190307',
                'name' => 'Villa Rica',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => '190308',
                'name' => 'Constitución',
                'region_id' => '190000',
                'province_id' => '190300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => '200101',
                'name' => 'Piura',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => '200104',
                'name' => 'Castilla',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => '200105',
                'name' => 'Catacaos',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => '200107',
                'name' => 'Cura Mori',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => '200108',
                'name' => 'El Tallan',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => '200109',
                'name' => 'La Arena',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => '200110',
                'name' => 'La Unión',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => '200111',
                'name' => 'Las Lomas',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => '200114',
                'name' => 'Tambo Grande',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => '200115',
                'name' => 'Veintiseis de Octubre',
                'region_id' => '200000',
                'province_id' => '200100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => '200201',
                'name' => 'Ayabaca',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => '200202',
                'name' => 'Frias',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => '200203',
                'name' => 'Jilili',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => '200204',
                'name' => 'Lagunas',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => '200205',
                'name' => 'Montero',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => '200206',
                'name' => 'Pacaipampa',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => '200207',
                'name' => 'Paimas',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => '200208',
                'name' => 'Sapillica',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => '200209',
                'name' => 'Sicchez',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => '200210',
                'name' => 'Suyo',
                'region_id' => '200000',
                'province_id' => '200200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => '200301',
                'name' => 'Huancabamba',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => '200302',
                'name' => 'Canchaque',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => '200303',
                'name' => 'El Carmen de la Frontera',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => '200304',
                'name' => 'Huarmaca',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => '200305',
                'name' => 'Lalaquiz',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => '200306',
                'name' => 'San Miguel de El Faique',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => '200307',
                'name' => 'Sondor',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => '200308',
                'name' => 'Sondorillo',
                'region_id' => '200000',
                'province_id' => '200300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => '200401',
                'name' => 'Chulucanas',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => '200402',
                'name' => 'Buenos Aires',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => '200403',
                'name' => 'Chalaco',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => '200404',
                'name' => 'La Matanza',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => '200405',
                'name' => 'Morropon',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => '200406',
                'name' => 'Salitral',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => '200407',
                'name' => 'San Juan de Bigote',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => '200408',
                'name' => 'Santa Catalina de Mossa',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => '200409',
                'name' => 'Santo Domingo',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => '200410',
                'name' => 'Yamango',
                'region_id' => '200000',
                'province_id' => '200400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => '200501',
                'name' => 'Paita',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => '200502',
                'name' => 'Amotape',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => '200503',
                'name' => 'Arenal',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => '200504',
                'name' => 'Colan',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => '200505',
                'name' => 'La Huaca',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => '200506',
                'name' => 'Tamarindo',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => '200507',
                'name' => 'Vichayal',
                'region_id' => '200000',
                'province_id' => '200500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => '200601',
                'name' => 'Sullana',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => '200602',
                'name' => 'Bellavista',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => '200603',
                'name' => 'Ignacio Escudero',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => '200604',
                'name' => 'Lancones',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => '200605',
                'name' => 'Marcavelica',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => '200606',
                'name' => 'Miguel Checa',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => '200607',
                'name' => 'Querecotillo',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => '200608',
                'name' => 'Salitral',
                'region_id' => '200000',
                'province_id' => '200600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => '200701',
                'name' => 'Pariñas',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => '200702',
                'name' => 'El Alto',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => '200703',
                'name' => 'La Brea',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => '200704',
                'name' => 'Lobitos',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => '200705',
                'name' => 'Los Organos',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => '200706',
                'name' => 'Mancora',
                'region_id' => '200000',
                'province_id' => '200700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => '200801',
                'name' => 'Sechura',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => '200802',
                'name' => 'Bellavista de la Unión',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => '200803',
                'name' => 'Bernal',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => '200804',
                'name' => 'Cristo Nos Valga',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => '200805',
                'name' => 'Vice',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => '200806',
                'name' => 'Rinconada Llicuar',
                'region_id' => '200000',
                'province_id' => '200800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => '210101',
                'name' => 'Puno',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => '210102',
                'name' => 'Acora',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => '210103',
                'name' => 'Amantani',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => '210104',
                'name' => 'Atuncolla',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => '210105',
                'name' => 'Capachica',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => '210106',
                'name' => 'Chucuito',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => '210107',
                'name' => 'Coata',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => '210108',
                'name' => 'Huata',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => '210109',
                'name' => 'Mañazo',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => '210110',
                'name' => 'Paucarcolla',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => '210111',
                'name' => 'Pichacani',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            140 => 
            array (
                'id' => '210112',
                'name' => 'Plateria',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            141 => 
            array (
                'id' => '210113',
                'name' => 'San Antonio',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            142 => 
            array (
                'id' => '210114',
                'name' => 'Tiquillaca',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            143 => 
            array (
                'id' => '210115',
                'name' => 'Vilque',
                'region_id' => '210000',
                'province_id' => '210100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            144 => 
            array (
                'id' => '210201',
                'name' => 'Azángaro',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            145 => 
            array (
                'id' => '210202',
                'name' => 'Achaya',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            146 => 
            array (
                'id' => '210203',
                'name' => 'Arapa',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            147 => 
            array (
                'id' => '210204',
                'name' => 'Asillo',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            148 => 
            array (
                'id' => '210205',
                'name' => 'Caminaca',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            149 => 
            array (
                'id' => '210206',
                'name' => 'Chupa',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            150 => 
            array (
                'id' => '210207',
                'name' => 'José Domingo Choquehuanca',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            151 => 
            array (
                'id' => '210208',
                'name' => 'Muñani',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            152 => 
            array (
                'id' => '210209',
                'name' => 'Potoni',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            153 => 
            array (
                'id' => '210210',
                'name' => 'Saman',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            154 => 
            array (
                'id' => '210211',
                'name' => 'San Anton',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            155 => 
            array (
                'id' => '210212',
                'name' => 'San José',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            156 => 
            array (
                'id' => '210213',
                'name' => 'San Juan de Salinas',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            157 => 
            array (
                'id' => '210214',
                'name' => 'Santiago de Pupuja',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            158 => 
            array (
                'id' => '210215',
                'name' => 'Tirapata',
                'region_id' => '210000',
                'province_id' => '210200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            159 => 
            array (
                'id' => '210301',
                'name' => 'Macusani',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            160 => 
            array (
                'id' => '210302',
                'name' => 'Ajoyani',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            161 => 
            array (
                'id' => '210303',
                'name' => 'Ayapata',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            162 => 
            array (
                'id' => '210304',
                'name' => 'Coasa',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            163 => 
            array (
                'id' => '210305',
                'name' => 'Corani',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            164 => 
            array (
                'id' => '210306',
                'name' => 'Crucero',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            165 => 
            array (
                'id' => '210307',
                'name' => 'Ituata',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            166 => 
            array (
                'id' => '210308',
                'name' => 'Ollachea',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            167 => 
            array (
                'id' => '210309',
                'name' => 'San Gaban',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            168 => 
            array (
                'id' => '210310',
                'name' => 'Usicayos',
                'region_id' => '210000',
                'province_id' => '210300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            169 => 
            array (
                'id' => '210401',
                'name' => 'Juli',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            170 => 
            array (
                'id' => '210402',
                'name' => 'Desaguadero',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            171 => 
            array (
                'id' => '210403',
                'name' => 'Huacullani',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            172 => 
            array (
                'id' => '210404',
                'name' => 'Kelluyo',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            173 => 
            array (
                'id' => '210405',
                'name' => 'Pisacoma',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            174 => 
            array (
                'id' => '210406',
                'name' => 'Pomata',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            175 => 
            array (
                'id' => '210407',
                'name' => 'Zepita',
                'region_id' => '210000',
                'province_id' => '210400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            176 => 
            array (
                'id' => '210501',
                'name' => 'Ilave',
                'region_id' => '210000',
                'province_id' => '210500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            177 => 
            array (
                'id' => '210502',
                'name' => 'Capazo',
                'region_id' => '210000',
                'province_id' => '210500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            178 => 
            array (
                'id' => '210503',
                'name' => 'Pilcuyo',
                'region_id' => '210000',
                'province_id' => '210500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            179 => 
            array (
                'id' => '210504',
                'name' => 'Santa Rosa',
                'region_id' => '210000',
                'province_id' => '210500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            180 => 
            array (
                'id' => '210505',
                'name' => 'Conduriri',
                'region_id' => '210000',
                'province_id' => '210500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            181 => 
            array (
                'id' => '210601',
                'name' => 'Huancane',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            182 => 
            array (
                'id' => '210602',
                'name' => 'Cojata',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            183 => 
            array (
                'id' => '210603',
                'name' => 'Huatasani',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            184 => 
            array (
                'id' => '210604',
                'name' => 'Inchupalla',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            185 => 
            array (
                'id' => '210605',
                'name' => 'Pusi',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            186 => 
            array (
                'id' => '210606',
                'name' => 'Rosaspata',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            187 => 
            array (
                'id' => '210607',
                'name' => 'Taraco',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            188 => 
            array (
                'id' => '210608',
                'name' => 'Vilque Chico',
                'region_id' => '210000',
                'province_id' => '210600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            189 => 
            array (
                'id' => '210701',
                'name' => 'Lampa',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            190 => 
            array (
                'id' => '210702',
                'name' => 'Cabanilla',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            191 => 
            array (
                'id' => '210703',
                'name' => 'Calapuja',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            192 => 
            array (
                'id' => '210704',
                'name' => 'Nicasio',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            193 => 
            array (
                'id' => '210705',
                'name' => 'Ocuviri',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            194 => 
            array (
                'id' => '210706',
                'name' => 'Palca',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            195 => 
            array (
                'id' => '210707',
                'name' => 'Paratia',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            196 => 
            array (
                'id' => '210708',
                'name' => 'Pucara',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            197 => 
            array (
                'id' => '210709',
                'name' => 'Santa Lucia',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            198 => 
            array (
                'id' => '210710',
                'name' => 'Vilavila',
                'region_id' => '210000',
                'province_id' => '210700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            199 => 
            array (
                'id' => '210801',
                'name' => 'Ayaviri',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            200 => 
            array (
                'id' => '210802',
                'name' => 'Antauta',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            201 => 
            array (
                'id' => '210803',
                'name' => 'Cupi',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            202 => 
            array (
                'id' => '210804',
                'name' => 'Llalli',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            203 => 
            array (
                'id' => '210805',
                'name' => 'Macari',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            204 => 
            array (
                'id' => '210806',
                'name' => 'Nuñoa',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            205 => 
            array (
                'id' => '210807',
                'name' => 'Orurillo',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            206 => 
            array (
                'id' => '210808',
                'name' => 'Santa Rosa',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            207 => 
            array (
                'id' => '210809',
                'name' => 'Umachiri',
                'region_id' => '210000',
                'province_id' => '210800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            208 => 
            array (
                'id' => '210901',
                'name' => 'Moho',
                'region_id' => '210000',
                'province_id' => '210900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            209 => 
            array (
                'id' => '210902',
                'name' => 'Conima',
                'region_id' => '210000',
                'province_id' => '210900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            210 => 
            array (
                'id' => '210903',
                'name' => 'Huayrapata',
                'region_id' => '210000',
                'province_id' => '210900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            211 => 
            array (
                'id' => '210904',
                'name' => 'Tilali',
                'region_id' => '210000',
                'province_id' => '210900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            212 => 
            array (
                'id' => '211001',
                'name' => 'Putina',
                'region_id' => '210000',
                'province_id' => '211000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            213 => 
            array (
                'id' => '211002',
                'name' => 'Ananea',
                'region_id' => '210000',
                'province_id' => '211000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            214 => 
            array (
                'id' => '211003',
                'name' => 'Pedro Vilca Apaza',
                'region_id' => '210000',
                'province_id' => '211000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            215 => 
            array (
                'id' => '211004',
                'name' => 'Quilcapuncu',
                'region_id' => '210000',
                'province_id' => '211000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            216 => 
            array (
                'id' => '211005',
                'name' => 'Sina',
                'region_id' => '210000',
                'province_id' => '211000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            217 => 
            array (
                'id' => '211101',
                'name' => 'Juliaca',
                'region_id' => '210000',
                'province_id' => '211100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            218 => 
            array (
                'id' => '211102',
                'name' => 'Cabana',
                'region_id' => '210000',
                'province_id' => '211100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            219 => 
            array (
                'id' => '211103',
                'name' => 'Cabanillas',
                'region_id' => '210000',
                'province_id' => '211100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            220 => 
            array (
                'id' => '211104',
                'name' => 'Caracoto',
                'region_id' => '210000',
                'province_id' => '211100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            221 => 
            array (
                'id' => '211105',
                'name' => 'San Miguel',
                'region_id' => '210000',
                'province_id' => '211100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            222 => 
            array (
                'id' => '211201',
                'name' => 'Sandia',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            223 => 
            array (
                'id' => '211202',
                'name' => 'Cuyocuyo',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            224 => 
            array (
                'id' => '211203',
                'name' => 'Limbani',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            225 => 
            array (
                'id' => '211204',
                'name' => 'Patambuco',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            226 => 
            array (
                'id' => '211205',
                'name' => 'Phara',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            227 => 
            array (
                'id' => '211206',
                'name' => 'Quiaca',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            228 => 
            array (
                'id' => '211207',
                'name' => 'San Juan del Oro',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            229 => 
            array (
                'id' => '211208',
                'name' => 'Yanahuaya',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            230 => 
            array (
                'id' => '211209',
                'name' => 'Alto Inambari',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            231 => 
            array (
                'id' => '211210',
                'name' => 'San Pedro de Putina Punco',
                'region_id' => '210000',
                'province_id' => '211200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            232 => 
            array (
                'id' => '211301',
                'name' => 'Yunguyo',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            233 => 
            array (
                'id' => '211302',
                'name' => 'Anapia',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            234 => 
            array (
                'id' => '211303',
                'name' => 'Copani',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            235 => 
            array (
                'id' => '211304',
                'name' => 'Cuturapi',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            236 => 
            array (
                'id' => '211305',
                'name' => 'Ollaraya',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            237 => 
            array (
                'id' => '211306',
                'name' => 'Tinicachi',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            238 => 
            array (
                'id' => '211307',
                'name' => 'Unicachi',
                'region_id' => '210000',
                'province_id' => '211300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            239 => 
            array (
                'id' => '220101',
                'name' => 'Moyobamba',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            240 => 
            array (
                'id' => '220102',
                'name' => 'Calzada',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            241 => 
            array (
                'id' => '220103',
                'name' => 'Habana',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            242 => 
            array (
                'id' => '220104',
                'name' => 'Jepelacio',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            243 => 
            array (
                'id' => '220105',
                'name' => 'Soritor',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            244 => 
            array (
                'id' => '220106',
                'name' => 'Yantalo',
                'region_id' => '220000',
                'province_id' => '220100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            245 => 
            array (
                'id' => '220201',
                'name' => 'Bellavista',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            246 => 
            array (
                'id' => '220202',
                'name' => 'Alto Biavo',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            247 => 
            array (
                'id' => '220203',
                'name' => 'Bajo Biavo',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            248 => 
            array (
                'id' => '220204',
                'name' => 'Huallaga',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            249 => 
            array (
                'id' => '220205',
                'name' => 'San Pablo',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            250 => 
            array (
                'id' => '220206',
                'name' => 'San Rafael',
                'region_id' => '220000',
                'province_id' => '220200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            251 => 
            array (
                'id' => '220301',
                'name' => 'San José de Sisa',
                'region_id' => '220000',
                'province_id' => '220300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            252 => 
            array (
                'id' => '220302',
                'name' => 'Agua Blanca',
                'region_id' => '220000',
                'province_id' => '220300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            253 => 
            array (
                'id' => '220303',
                'name' => 'San Martín',
                'region_id' => '220000',
                'province_id' => '220300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            254 => 
            array (
                'id' => '220304',
                'name' => 'Santa Rosa',
                'region_id' => '220000',
                'province_id' => '220300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            255 => 
            array (
                'id' => '220305',
                'name' => 'Shatoja',
                'region_id' => '220000',
                'province_id' => '220300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            256 => 
            array (
                'id' => '220401',
                'name' => 'Saposoa',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            257 => 
            array (
                'id' => '220402',
                'name' => 'Alto Saposoa',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            258 => 
            array (
                'id' => '220403',
                'name' => 'El Eslabón',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            259 => 
            array (
                'id' => '220404',
                'name' => 'Piscoyacu',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            260 => 
            array (
                'id' => '220405',
                'name' => 'Sacanche',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            261 => 
            array (
                'id' => '220406',
                'name' => 'Tingo de Saposoa',
                'region_id' => '220000',
                'province_id' => '220400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            262 => 
            array (
                'id' => '220501',
                'name' => 'Lamas',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            263 => 
            array (
                'id' => '220502',
                'name' => 'Alonso de Alvarado',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            264 => 
            array (
                'id' => '220503',
                'name' => 'Barranquita',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            265 => 
            array (
                'id' => '220504',
                'name' => 'Caynarachi',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            266 => 
            array (
                'id' => '220505',
                'name' => 'Cuñumbuqui',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            267 => 
            array (
                'id' => '220506',
                'name' => 'Pinto Recodo',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            268 => 
            array (
                'id' => '220507',
                'name' => 'Rumisapa',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            269 => 
            array (
                'id' => '220508',
                'name' => 'San Roque de Cumbaza',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            270 => 
            array (
                'id' => '220509',
                'name' => 'Shanao',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            271 => 
            array (
                'id' => '220510',
                'name' => 'Tabalosos',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            272 => 
            array (
                'id' => '220511',
                'name' => 'Zapatero',
                'region_id' => '220000',
                'province_id' => '220500',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            273 => 
            array (
                'id' => '220601',
                'name' => 'Juanjuí',
                'region_id' => '220000',
                'province_id' => '220600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            274 => 
            array (
                'id' => '220602',
                'name' => 'Campanilla',
                'region_id' => '220000',
                'province_id' => '220600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            275 => 
            array (
                'id' => '220603',
                'name' => 'Huicungo',
                'region_id' => '220000',
                'province_id' => '220600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            276 => 
            array (
                'id' => '220604',
                'name' => 'Pachiza',
                'region_id' => '220000',
                'province_id' => '220600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            277 => 
            array (
                'id' => '220605',
                'name' => 'Pajarillo',
                'region_id' => '220000',
                'province_id' => '220600',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            278 => 
            array (
                'id' => '220701',
                'name' => 'Picota',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            279 => 
            array (
                'id' => '220702',
                'name' => 'Buenos Aires',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            280 => 
            array (
                'id' => '220703',
                'name' => 'Caspisapa',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            281 => 
            array (
                'id' => '220704',
                'name' => 'Pilluana',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            282 => 
            array (
                'id' => '220705',
                'name' => 'Pucacaca',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            283 => 
            array (
                'id' => '220706',
                'name' => 'San Cristóbal',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            284 => 
            array (
                'id' => '220707',
                'name' => 'San Hilarión',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            285 => 
            array (
                'id' => '220708',
                'name' => 'Shamboyacu',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            286 => 
            array (
                'id' => '220709',
                'name' => 'Tingo de Ponasa',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            287 => 
            array (
                'id' => '220710',
                'name' => 'Tres Unidos',
                'region_id' => '220000',
                'province_id' => '220700',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            288 => 
            array (
                'id' => '220801',
                'name' => 'Rioja',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            289 => 
            array (
                'id' => '220802',
                'name' => 'Awajun',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            290 => 
            array (
                'id' => '220803',
                'name' => 'Elías Soplin Vargas',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            291 => 
            array (
                'id' => '220804',
                'name' => 'Nueva Cajamarca',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            292 => 
            array (
                'id' => '220805',
                'name' => 'Pardo Miguel',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            293 => 
            array (
                'id' => '220806',
                'name' => 'Posic',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            294 => 
            array (
                'id' => '220807',
                'name' => 'San Fernando',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            295 => 
            array (
                'id' => '220808',
                'name' => 'Yorongos',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            296 => 
            array (
                'id' => '220809',
                'name' => 'Yuracyacu',
                'region_id' => '220000',
                'province_id' => '220800',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            297 => 
            array (
                'id' => '220901',
                'name' => 'Tarapoto',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            298 => 
            array (
                'id' => '220902',
                'name' => 'Alberto Leveau',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            299 => 
            array (
                'id' => '220903',
                'name' => 'Cacatachi',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            300 => 
            array (
                'id' => '220904',
                'name' => 'Chazuta',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            301 => 
            array (
                'id' => '220905',
                'name' => 'Chipurana',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            302 => 
            array (
                'id' => '220906',
                'name' => 'El Porvenir',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            303 => 
            array (
                'id' => '220907',
                'name' => 'Huimbayoc',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            304 => 
            array (
                'id' => '220908',
                'name' => 'Juan Guerra',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            305 => 
            array (
                'id' => '220909',
                'name' => 'La Banda de Shilcayo',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            306 => 
            array (
                'id' => '220910',
                'name' => 'Morales',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            307 => 
            array (
                'id' => '220911',
                'name' => 'Papaplaya',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            308 => 
            array (
                'id' => '220912',
                'name' => 'San Antonio',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            309 => 
            array (
                'id' => '220913',
                'name' => 'Sauce',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            310 => 
            array (
                'id' => '220914',
                'name' => 'Shapaja',
                'region_id' => '220000',
                'province_id' => '220900',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            311 => 
            array (
                'id' => '221001',
                'name' => 'Tocache',
                'region_id' => '220000',
                'province_id' => '221000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            312 => 
            array (
                'id' => '221002',
                'name' => 'Nuevo Progreso',
                'region_id' => '220000',
                'province_id' => '221000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            313 => 
            array (
                'id' => '221003',
                'name' => 'Polvora',
                'region_id' => '220000',
                'province_id' => '221000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            314 => 
            array (
                'id' => '221004',
                'name' => 'Shunte',
                'region_id' => '220000',
                'province_id' => '221000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            315 => 
            array (
                'id' => '221005',
                'name' => 'Uchiza',
                'region_id' => '220000',
                'province_id' => '221000',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            316 => 
            array (
                'id' => '230101',
                'name' => 'Tacna',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            317 => 
            array (
                'id' => '230102',
                'name' => 'Alto de la Alianza',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            318 => 
            array (
                'id' => '230103',
                'name' => 'Calana',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            319 => 
            array (
                'id' => '230104',
                'name' => 'Ciudad Nueva',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            320 => 
            array (
                'id' => '230105',
                'name' => 'Inclan',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            321 => 
            array (
                'id' => '230106',
                'name' => 'Pachia',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            322 => 
            array (
                'id' => '230107',
                'name' => 'Palca',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            323 => 
            array (
                'id' => '230108',
                'name' => 'Pocollay',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            324 => 
            array (
                'id' => '230109',
                'name' => 'Sama',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            325 => 
            array (
                'id' => '230110',
                'name' => 'Coronel Gregorio Albarracín Lanchipa',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            326 => 
            array (
                'id' => '230111',
                'name' => 'La Yarada los Palos',
                'region_id' => '230000',
                'province_id' => '230100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            327 => 
            array (
                'id' => '230201',
                'name' => 'Candarave',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            328 => 
            array (
                'id' => '230202',
                'name' => 'Cairani',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            329 => 
            array (
                'id' => '230203',
                'name' => 'Camilaca',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            330 => 
            array (
                'id' => '230204',
                'name' => 'Curibaya',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            331 => 
            array (
                'id' => '230205',
                'name' => 'Huanuara',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            332 => 
            array (
                'id' => '230206',
                'name' => 'Quilahuani',
                'region_id' => '230000',
                'province_id' => '230200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            333 => 
            array (
                'id' => '230301',
                'name' => 'Locumba',
                'region_id' => '230000',
                'province_id' => '230300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            334 => 
            array (
                'id' => '230302',
                'name' => 'Ilabaya',
                'region_id' => '230000',
                'province_id' => '230300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            335 => 
            array (
                'id' => '230303',
                'name' => 'Ite',
                'region_id' => '230000',
                'province_id' => '230300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            336 => 
            array (
                'id' => '230401',
                'name' => 'Tarata',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            337 => 
            array (
                'id' => '230402',
                'name' => 'Héroes Albarracín',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            338 => 
            array (
                'id' => '230403',
                'name' => 'Estique',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            339 => 
            array (
                'id' => '230404',
                'name' => 'Estique-Pampa',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            340 => 
            array (
                'id' => '230405',
                'name' => 'Sitajara',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            341 => 
            array (
                'id' => '230406',
                'name' => 'Susapaya',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            342 => 
            array (
                'id' => '230407',
                'name' => 'Tarucachi',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            343 => 
            array (
                'id' => '230408',
                'name' => 'Ticaco',
                'region_id' => '230000',
                'province_id' => '230400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            344 => 
            array (
                'id' => '240101',
                'name' => 'Tumbes',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            345 => 
            array (
                'id' => '240102',
                'name' => 'Corrales',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            346 => 
            array (
                'id' => '240103',
                'name' => 'La Cruz',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            347 => 
            array (
                'id' => '240104',
                'name' => 'Pampas de Hospital',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            348 => 
            array (
                'id' => '240105',
                'name' => 'San Jacinto',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            349 => 
            array (
                'id' => '240106',
                'name' => 'San Juan de la Virgen',
                'region_id' => '240000',
                'province_id' => '240100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            350 => 
            array (
                'id' => '240201',
                'name' => 'Zorritos',
                'region_id' => '240000',
                'province_id' => '240200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            351 => 
            array (
                'id' => '240202',
                'name' => 'Casitas',
                'region_id' => '240000',
                'province_id' => '240200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            352 => 
            array (
                'id' => '240203',
                'name' => 'Canoas de Punta Sal',
                'region_id' => '240000',
                'province_id' => '240200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            353 => 
            array (
                'id' => '240301',
                'name' => 'Zarumilla',
                'region_id' => '240000',
                'province_id' => '240300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            354 => 
            array (
                'id' => '240302',
                'name' => 'Aguas Verdes',
                'region_id' => '240000',
                'province_id' => '240300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            355 => 
            array (
                'id' => '240303',
                'name' => 'Matapalo',
                'region_id' => '240000',
                'province_id' => '240300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            356 => 
            array (
                'id' => '240304',
                'name' => 'Papayal',
                'region_id' => '240000',
                'province_id' => '240300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            357 => 
            array (
                'id' => '250101',
                'name' => 'Calleria',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            358 => 
            array (
                'id' => '250102',
                'name' => 'Campoverde',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            359 => 
            array (
                'id' => '250103',
                'name' => 'Iparia',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            360 => 
            array (
                'id' => '250104',
                'name' => 'Masisea',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            361 => 
            array (
                'id' => '250105',
                'name' => 'Yarinacocha',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            362 => 
            array (
                'id' => '250106',
                'name' => 'Nueva Requena',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            363 => 
            array (
                'id' => '250107',
                'name' => 'Manantay',
                'region_id' => '250000',
                'province_id' => '250100',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            364 => 
            array (
                'id' => '250201',
                'name' => 'Raymondi',
                'region_id' => '250000',
                'province_id' => '250200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            365 => 
            array (
                'id' => '250202',
                'name' => 'Sepahua',
                'region_id' => '250000',
                'province_id' => '250200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            366 => 
            array (
                'id' => '250203',
                'name' => 'Tahuania',
                'region_id' => '250000',
                'province_id' => '250200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            367 => 
            array (
                'id' => '250204',
                'name' => 'Yurua',
                'region_id' => '250000',
                'province_id' => '250200',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            368 => 
            array (
                'id' => '250301',
                'name' => 'Padre Abad',
                'region_id' => '250000',
                'province_id' => '250300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            369 => 
            array (
                'id' => '250302',
                'name' => 'Irazola',
                'region_id' => '250000',
                'province_id' => '250300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            370 => 
            array (
                'id' => '250303',
                'name' => 'Curimana',
                'region_id' => '250000',
                'province_id' => '250300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            371 => 
            array (
                'id' => '250304',
                'name' => 'Neshuya',
                'region_id' => '250000',
                'province_id' => '250300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            372 => 
            array (
                'id' => '250305',
                'name' => 'Alexander Von Humboldt',
                'region_id' => '250000',
                'province_id' => '250300',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            373 => 
            array (
                'id' => '250401',
                'name' => 'Purús',
                'region_id' => '250000',
                'province_id' => '250400',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}