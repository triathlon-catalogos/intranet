<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\VerifiedUser;
use App\Catalog;
use Carbon\Carbon;
use Goutte;

class IntranetController extends Controller
{
    public function index() 
    {
        $user = Auth::user();
        $validUser = false;
        if ($user->type == "customer") {
            $verified = VerifiedUser::where("user_id", $user->id)->latest()->first();
            if ($verified != null) {
                if ($verified->count() > 0) {
                    $catalog = Catalog::find($verified->catalog_id);
                    if ($catalog->due_date >= Carbon::now()) {
                        $validUser = true;
                    }
                }
            }
        } else {
            $validUser = true;
        }
        
        $crawler = Goutte::request('GET', 'http://catalogosdeportivos.pe/catalogo/');
        $lastCatalogLink = $crawler->filter('.catalogo a')->first()->attr('href');
        
    	return view('home', [
    	    "is_valid" => $validUser,
    	    "catalog_url" => $lastCatalogLink,
    	]);
    }
}
