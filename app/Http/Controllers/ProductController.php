<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function stock() 
    {
        $cart = session('cart');
        $cartTotalQty = 0;
        if ($this->existsCart()) {
            $products = $cart["products"];
            foreach ($products as $key=>$value) {
                $cartTotalQty += $value["Qty"];
            }    
        }
        // session()->forget('cart');
    	return view('products.stock', compact("cartTotalQty"));
    }
    
    private function existsCart() {
        $cart = session('cart');
        return $cart != null && isset($cart["products"]);
    }
}
