<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\OrderDetail;
use App\Region;
use App\Province;
use App\District;
use App\Freight;
use JavaScript;

class OrderController extends Controller
{
    public function history() 
    {
        $customerNo = Auth::user()->extid;
        $orders = Order::where('customer', $customerNo)
                    ->orderBy('order_date', 'desc')
                    ->get();
        
    	return view('orders.history', compact('orders'));
    }
    
    public function summary() 
    {
        $cart = session('cart');
        $products = [];
        if ($this->existsCart()) {
            $products = $cart["products"];
        }
        
    	return view('orders.summary', compact("products"));
    }
    
    public function checkout() 
    {
        if (!$this->existsCart()) {
            return redirect('orders/summary');
        }
        
        $cart = session('cart');
        $customer = $cart['customer'];
        $products = $cart['products'];
        $docNumber = $this->getDocNumber();
        $payTotal = 0;
        $totalQty = 0;
        foreach ($products as $product) {
            $totalQty += $product['Qty'];
            $payTotal += ($product['Qty'] * $product['RetailPrice']);
        }
        
        $regions = Region::all();
        
        JavaScript::put([
            'freights' => Freight::all(),
            'totalQty' => $totalQty,
        ]);
        
    	return view(
    	    'orders.checkout', 
    	    compact(
    	        'customer', 
    	        'docNumber', 
    	        'products', 
    	        'payTotal', 
    	        'regions',
    	        'freights'
    	   )
    	);
    }
    
    public function cart(Request $request) 
    {
        try {
            $cart = $request->get('cart');
            session()->put('cart', $cart);    
            
	    	return response()->json(["result" => "success"]);
        } catch(Exception $e) {
    		return response()->json(["result" => "failed", "error" => $e->getMessage()]);
        }
    }
    
    public function complete(Request $request)
    {
        $cart = session('cart');
        $products = $cart['products'];
        
        $payTotal = 0;
        foreach ($products as $product) {
            $payTotal += ($product['Qty'] * $product['RetailPrice']);
        }
        
        session()->forget('cart');
        return view('orders.complete', ['payTotal' => $payTotal]);
    }
    
    public function save(Request $request)
    {
        try {
            $order = new Order;
            $order->customer = $request->CustomerNo;
            $order->order_date = $request->OrderDate;
            $order->external_id = $request->ExtId;
            $order->shipping = $request->ShippingTotal;
            $order->subtotal = $request->SubTotal;
            $order->tax = $request->TaxTotal;
            $order->total = $request->PayTotal;
            $order->items = $request->LineCount;
            $order->status = 'pending';
            $order->save();
            
            $orderDetails = [];
            $lines = $request->Lines;
            
            for($i = 0; $i < count($lines); $i++) {
                $line = $lines[$i];
                
                array_push($orderDetails, new OrderDetail([
                    'item_number' => $i + 1,
                    'sku' => $line['SKU'],
                    'quantity' => $line['Qty'],
                    'retail_price' => $line['RetailPrice'],
                    'tax_amount' => $line['TaxAmount'],
                    'price' => $line['ExtRetailPriceWTax'],
                ]));
            }
            
            $order->lines()->saveMany($orderDetails);
            
            return response()->json(["result" => "success", "data" => $request->all()]);
        } catch (Exception $e) {
            return response()->json(["result" => "failed", "error" => $e->getMessage()]);
        }
    }
    
    private function existsCart() 
    {
        $cart = session('cart');
        return $cart != null && isset($cart["products"]);
    }
    
    private function getDocNumber() 
    {
        $lastId = rand(1, 100);
        return 'WEBCAT-' . $lastId;
    }
}
