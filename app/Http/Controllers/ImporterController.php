<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ImporterController extends Controller
{
    public function index()
    {
    	return view("importer.index");
    }

    public function importUsers(Request $request) 
    {
    	try {
	    	$users = json_decode($request->input('users'));
	    	foreach ($users->customers as $key => $customer) {
	    		$user = User::where('dni', $customer->dni)->first();
	    		if ($user == null) {
	    			$user = new User();
	    			$user->name = $customer->firstName;
	    			$user->lastname = $customer->lastName;
	    			$user->username = $customer->dni;
	    			$user->email = $customer->dni . "@catalogothn.com.pe";
	    			$user->password = bcrypt($customer->dni);
	    			$user->dni = $customer->dni;
	    			$user->ruc = $customer->ruc;
	    			$user->extid = $customer->customerNo;
	    			$user->type = "customer";
	    			$user->save();
	    		}
	    	}
	    	foreach ($users->employees as $key => $employee) {
	    		$user = User::where('username', $employee->employeeCode)->first();
	    		if ($user == null) {
	    			$user = new User();
	    			$user->name = $employee->firstName;
	    			$user->lastname = $employee->lastName;
	    			$user->username = strtolower($employee->employeeCode);
	    			$user->email = strtolower($employee->employeeCode) . "@catalogothn.com.pe";
	    			$user->password = bcrypt(strtolower($employee->employeeCode));
	    			$user->dni = strtolower($employee->employeeCode);
	    			$user->ruc = "";
	    			$user->extid = $employee->employeeCode;
	    			$user->type = "employee";
	    			$user->save();
	    		}
	    	}
	    	return response()->json(["result" => "success", "data" => $users]);
    	} catch (Exception $e) {
    		return response()->json(["result" => "failed", "error" => $e->getMessage()]);
    	}
    }
    
    public function importMassiveUsers(Request $request) 
    {
		$data = $request->json()->all();
    	try {
	    	$users = $data['users'];
	    	$customers = $users['customers'];
	    	for ($i = 0, $l = count($customers); $i < $l; $i++) {
	    		$customer = $customers[$i];
	    		$user = User::where('dni', $customer['dni'])->first();
	    		if ($user == null) {
	    			$user = new User();
	    		}
	    		$user->name = $customer['firstName'];
    			$user->lastname = $customer['lastName'];
    			$user->username = $customer['dni'];
    			$user->email = $customer['dni'] . "@catalogothn.com.pe";
    			$user->password = bcrypt($customer['dni']);
    			$user->dni = $customer['dni'];
    			$user->ruc = $customer['ruc'];
    			$user->extid = $customer['customerNo'];
    			$user->type = "customer";
    			$user->save();
	    	}
	    	// foreach ($users->employees as $key => $employee) {
	    	// 	$user = User::where('username', $employee->employeeCode)->first();
	    	// 	if ($user == null) {
	    	// 		$user = new User();
	    	// 		$user->name = $employee->firstName;
	    	// 		$user->lastname = $employee->lastName;
	    	// 		$user->username = strtolower($employee->employeeCode);
	    	// 		$user->email = strtolower($employee->employeeCode) . "@catalogothn.com.pe";
	    	// 		$user->password = bcrypt(strtolower($employee->employeeCode));
	    	// 		$user->dni = strtolower($employee->employeeCode);
	    	// 		$user->ruc = "";
	    	// 		$user->extid = $employee->employeeCode;
	    	// 		$user->type = "employee";
	    	// 		$user->save();
	    	// 	}
	    	// }
	    	return response()->json(["result" => "success", "data" => $users]);
    	} catch (Exception $e) {
    		return response()->json(["result" => "failed", "error" => $e->getMessage()]);
    	}
    }
}
