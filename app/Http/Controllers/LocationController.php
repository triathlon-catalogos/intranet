<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;
use App\Province;
use App\District;

class LocationController extends Controller
{
    public function getProvinces(Request $request) {
    	$region_id = $request->get('region_id');
    	
    	$provinces = Region::find($region_id)->provinces;
    	
    	return response()->json(
    		[ 
    			'data' => $provinces,
    		]
    	);
    }
    
    public function getDistricts(Request $request) {
    	$region_id = $request->get('region_id');
    	$province_id = $request->get('province_id');
    	
    	$districts = Region::find($region_id)->provinces()
    		->find($province_id)->districts;
    	
    	return response()->json(
    		[ 
    			'data' => $districts,
    		]
    	);
    }
}
