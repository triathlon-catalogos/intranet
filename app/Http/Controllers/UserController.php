<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Catalog;
use App\VerifiedUser;

class UserController extends Controller
{
    public function verify() 
    {
    	return view('users.verify');
    }

    public function validateCatalog(Request $request) {
        try {
        	$receipt = $request->get('receipt');
        	$catalog = Catalog::where("product_code", $receipt["StyleName"])->latest()->first();
        	if ($catalog->count() > 0) {
        	    $userId = Auth::user()->id;
        	    $catalogId = $catalog->id;
        	    $verifiedUser = new VerifiedUser();
        	    $verifiedUser->user_id = $userId;
        	    $verifiedUser->catalog_id = $catalogId;
        	    $verifiedUser->is_valid = true;
        	    $verifiedUser->save();
        	    
    	    	return response()->json(["result" => "success", "data" => $verifiedUser]);
        	} else {
        		return response()->json(["result" => "failed", "error" => "El catálogo no ha sido configurado."]);
        	}
        } catch (Exception $e) {
            return response()->json(["result" => "failed", "error" => $e->getMessage()]);
        }
    }
}
