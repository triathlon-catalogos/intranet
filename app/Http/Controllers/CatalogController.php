<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;

class CatalogController extends Controller
{
    public function manage() 
    {
        $catalogs = Catalog::all();
    	return view("catalog.manage", compact("catalogs"));
    }
    
    public function save(Request $request)
    {
        try {
            $catalog = new Catalog();
            $catalogData = $request->get("catalog");
            $catalog->name = $catalogData["name"];
            $catalog->release_date = $catalogData["releaseDate"];
            $catalog->due_date = $catalogData["dueDate"];
            $catalog->product_code = $catalogData["productCode"];
            $catalog->save();
            
	    	return response()->json(["result" => "success", "data" => $catalog]);
    	} catch (Exception $e) {
    		return response()->json(["result" => "failed", "error" => $e->getMessage()]);
    	}
    }
    
    public function update(Request $request)
    {
        try {
            $catalogData = $request->get("catalog");
            $catalog = Catalog::find($catalogData["id"]);
            $catalog->name = $catalogData["name"];
            $catalog->release_date = $catalogData["releaseDate"];
            $catalog->due_date = $catalogData["dueDate"];
            $catalog->product_code = $catalogData["productCode"];
            $catalog->save();
            
	    	return response()->json(["result" => "success", "data" => $catalog]);
    	} catch (Exception $e) {
    		return response()->json(["result" => "failed", "error" => $e->getMessage()]);
    	}
    }
}
