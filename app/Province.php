<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    public $incrementing = false;
    
    protected $keyType = 'string';
    
    protected $fillable = [
        'name', 'region_id',
    ];
    
    public function districts() {
        return $this->hasMany('App\District', 'province_id');
    }

}
