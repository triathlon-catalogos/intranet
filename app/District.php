<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $incrementing = false;
    
    protected $keyType = 'string';
    
    protected $fillable = [
        'name', 'region_id', 'province_id'
    ];

}
