<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifiedUser extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'catalog_id', 'is_valid',
    ];

}
