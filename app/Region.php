<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $incrementing = false;
    
    protected $keyType = 'string';

    protected $fillable = [
        'id', 'name',
    ];
    
    public function provinces() {
        return $this->hasMany('App\Province', 'region_id');
    }

}
