<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $fillable = [
        'item_number', 'sku', 'quantity', 
        'retail_price', 'tax_amount', 'price',
    ];
}
